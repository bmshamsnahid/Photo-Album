export class Recruiter {
  constructor() {}
  _id: string;
  name: string;
  email: string;
  role: string;
  password: string;
  createdDate: Date;
}
