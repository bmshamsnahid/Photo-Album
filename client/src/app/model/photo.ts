export class Photo {
  _id: string;
  name: string;
  displayName: string;
  originalPath: string;
  watermarkedPath: string;
  views: number;
  likes: number;
  upVote: number;
  uploadDate: Date;
  tags: [string];
  albumId: string;
  ownerId: string;
  category: string;
  upVoterList: [string];
  voteAvailability: boolean;
}
