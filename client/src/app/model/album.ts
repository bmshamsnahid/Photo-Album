import {Photographer} from "./photographer";

export class Album {
  constructor() {}
  _id: string;
  name: string;
  description: string;
  views: number;
  category: string;
  upVote: number;
  uploadDate: Date;
  updateDate: [Date];
  tags: [string];
  ownerId: string;
  isPrivate: boolean;
  owner: Photographer;
}
