import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {ReactiveFormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import {NgxPaginationModule} from 'ngx-pagination';

import { AppComponent } from './app.component';
import {HttpClientModule} from '@angular/common/http';
import {FileUploadModule} from 'ng2-file-upload';
import {FileService} from './fiel-service';
import {NavbarComponent} from './components/Navbar/navbar.component';
import {HomeComponent} from './components/Home/home.component';
import {FooterComponent} from './components/Footer/footer.component';
import {AlbumsComponent} from './components/Albums/albums.component';
import {PhotoComponent} from './components/Photo/photo.component';
import {SearchComponent} from './components/Search/search.component';
import {DashboardComponent} from './components/Dashboard/dashboard.component';
import {LoginComponent} from './components/Login/login.component';
import {RegisterComponent} from './components/Register/register.component';
import {PhotographerService} from './services/photographer.service';
import {PhotoService} from './services/photo.service';
import {AlbumService} from './services/album.service';
import {UserAuthService} from './services/user-auth.service';
import {AuthGuardService} from './services/auth-guard.service';
import {DashboardService} from './services/dashboard.service';
import {RouterModule} from "@angular/router";
import {LogoutComponent} from "./components/logout.component";
import {AlbumComponent} from "./components/Album/album.component";
import {PhotographerComponent} from "./components/Photographer/photographer.component";
import {UploadComponent} from "./components/upload/upload.component";
import {PhotoUpload} from "./components/photoUpload/photoUpload";
import { ToastrService } from "./common/toastr.service";
import {FormsModule} from '@angular/forms';
import {StripeService} from "./common/stripe.service";
import {PhotographerRegisterComponent} from "./components/Register/photographer/photographer-register.component";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {PhotographerLoginComponent} from "./components/Login/photographer/photographer-login.component";
import {PhotographerAuthGuardService} from "./services/auth-guard/photographer-auth-guard.service";
import {PhotographerDashboardComponent} from "./components/Dashboard/photographer/photographer-dashboard.component";
import {RecruiterDashboardComponent} from "./components/Dashboard/recruiter/recruiter-dashboard.component";
import {SuperAdminDashboardComponent} from "./components/Dashboard/super-admin/super-admin-dashboard.component";
import {AdminDashboardComponent} from "./components/Dashboard/admin/admin-dashboard.component";
import {AdminAuthGuardService} from "./services/auth-guard/admin-auth-guard.service";
import {SuperAdminAuthGuardService} from "./services/auth-guard/super-admin-auth-guard.service";
import {RecruiterAuthGuardService} from "./services/auth-guard/recruiter-auth-guard.service";
import {RecruiterLoginComponent} from "./components/Login/recruiter/recruiter-login.component";
import {AdminLoginComponent} from "./components/Login/admin/admin-login.component";
import {SuperAdminLoginComponent} from "./components/Login/superAdmin/super-admin-login.component";
import {AdminRegisterComponent} from "./components/Register/admin/admin-register.component";
import {SuperAdminRegisterComponent} from "./components/Register/superAdmin/super-admin-register.component";
import {RecruiterRegisterComponent} from "./components/Register/recruiter/recruiter-register.component";
import {AdminService} from "./services/admin.service";
import {SuperAdminService} from "./services/super-admin.service";
import {RecruiterService} from "./services/recruiter.service";
import { LightboxModule } from 'angular2-lightbox';
import {CartComponent} from "./components/cart/cart.component";
import {PhotographerProfileComponent} from "./components/profile/photographerProfile/photographer-profile.component";
import {AdminProfileComponent} from "./components/profile/adminProfile/admin-profile.component";
import {SuperAdminProfileComponent} from "./components/profile/superAdminProfile/super-admin-profile.component";
import {RecruiterProfileComponent} from "./components/profile/recruiterProfile/recruiter-profile.component";
import {CategoryService} from "./services/category.service";
import {AlbumCreatorComponent} from "./components/AlbumCreateor/album-creator.component";
import {CategoryComponent} from "./components/category/category.component";
import {CategoryDisplayComponent} from "./components/display/categoryDisplay/categoryDisplay.component";
import {CategoryAlbumDisplayComponent} from "./components/display/categoryAlbum/categoryAlbumDisplay.component";
import {CartService} from "./services/cart.service";
import {BuyListComponent} from "./components/BuyList/buy-list.component";
import {BuyListService} from "./services/buyList.service";
import {AlbumPhotoDashboardComponent} from "./components/Dashboard/photographer/albumPhoto/album-photo-dashboard.component";
import {CustomService} from "./services/custom.service";
import {PhotosDisplayComponent} from "./components/Photos/photos-display.component";
import {ProfilesComponent} from "./components/Profiles/profiles.component";
import {PhotographerAlbumsComponent} from "./components/display/photographerAlbum/photographer-albums.component";
import {AdminDashboardPhotographerInfoComponent} from "./components/Dashboard/admin/PhotographerInfo/admin-dashboard-photographer-info.component";
import {SearchPhotoComponent} from "./components/search-item/search-photo/search-photo.component";
import {SearchAlbumComponent} from "./components/search-item/search-album/search-album.component";
import {SearchPhotographerComponent} from "./components/search-item/search-photographer/search-photographer.component";
import {SearchRecruiterComponent} from "./components/search-item/search-recruiter/search-recruiter.component";
import {SearchHelperService} from "./services/search-helper.service";
import {MessageService} from "./services/message.service";
import {MessageComponent} from "./components/message/message.component";
import {PhotographerPaymentService} from "./common/photographer-payment.service";


@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    HomeComponent,
    FooterComponent,
    AlbumsComponent,
    AlbumComponent,
    CategoryAlbumDisplayComponent,
    AlbumPhotoDashboardComponent,
    AlbumCreatorComponent,
    PhotoComponent,
    CategoryComponent,
    CategoryDisplayComponent,
    SearchComponent,
    SearchPhotoComponent,
    SearchAlbumComponent,
    SearchPhotographerComponent,
    SearchRecruiterComponent,
    CartComponent,
    BuyListComponent,
    DashboardComponent,
    LoginComponent,
    RegisterComponent,
    LogoutComponent,
    PhotographerComponent,
    UploadComponent,
    PhotoUpload,
    PhotographerProfileComponent,
    AdminProfileComponent,
    SuperAdminProfileComponent,
    RecruiterProfileComponent,
    PhotographerRegisterComponent,
    AdminRegisterComponent,
    SuperAdminRegisterComponent,
    RecruiterRegisterComponent,
    PhotographerLoginComponent,
    RecruiterLoginComponent,
    AdminLoginComponent,
    SuperAdminLoginComponent,
    PhotographerDashboardComponent,
    RecruiterDashboardComponent,
    AdminDashboardComponent,
    SuperAdminDashboardComponent,
    PhotosDisplayComponent,
    ProfilesComponent,
    PhotographerAlbumsComponent,
    AdminDashboardPhotographerInfoComponent,
    MessageComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    FileUploadModule,
    HttpClientModule,
    FileUploadModule,
    FormsModule,
    BrowserAnimationsModule,
    LightboxModule,
    NgxPaginationModule,
    RouterModule.forRoot([
      { path: 'home', component: HomeComponent, pathMatch: 'full' },
      { path: 'albumCreate', component: AlbumCreatorComponent },
      { path: 'album/:id', component: AlbumComponent },
      { path: 'album', component: AlbumsComponent },
      { path: 'categoryAlbumDisplay/:categoryId', component: CategoryAlbumDisplayComponent },
      { path: 'category', component: CategoryComponent },
      { path: 'categoryDisplay', component: CategoryDisplayComponent },
      { path: 'photographersAlbumDisplay/:photographerId', component: PhotographerAlbumsComponent },
      { path: 'photo/:id', component: PhotoComponent },
      { path: 'photos', component: PhotosDisplayComponent },
      { path: 'photo-upload/:id', component: PhotoUpload },
      { path: 'search', component: SearchComponent },
      { path: 'search-photo-keyword/:keyword', component: SearchPhotoComponent },
      { path: 'search-album-keyword/:keyword', component: SearchAlbumComponent },
      { path: 'search-photographer-keyword/:keyword', component: SearchPhotographerComponent },
      { path: 'search-recruiter-keyword/:keyword', component: SearchRecruiterComponent },
      { path: 'login', component: LoginComponent },
      { path: 'cart', canActivate: [ PhotographerAuthGuardService ], component: CartComponent },
      { path: 'buy-list', canActivate: [ PhotographerAuthGuardService ], component: BuyListComponent },
      { path: 'register', component: RegisterComponent },
      { path: 'profiles', component: ProfilesComponent },
      { path: 'photographer-profile/:photographerId', canActivate: [ PhotographerAuthGuardService ], component: PhotographerProfileComponent },
      { path: 'admin-profile', canActivate: [ AdminAuthGuardService ], component: AdminProfileComponent },
      { path: 'super-admin-profile', canActivate: [ SuperAdminAuthGuardService ], component: SuperAdminProfileComponent },
      { path: 'recruiter-profile', canActivate: [ RecruiterAuthGuardService ], component: RecruiterProfileComponent },
      { path: 'photographerProfile-register', component: PhotographerRegisterComponent },
      { path: 'admin-register', component: AdminRegisterComponent },
      { path: 'super-admin-register', component: SuperAdminRegisterComponent },
      { path: 'recruiterProfile-register', component: RecruiterRegisterComponent },
      { path: 'photographerProfile-login', component: PhotographerLoginComponent },
      { path: 'admin-login', component: AdminLoginComponent },
      { path: 'super-admin-login', component: SuperAdminLoginComponent },
      { path: 'recruiter-login', component: RecruiterLoginComponent },
      { path: 'photographerProfile-dashboard', canActivate: [ PhotographerAuthGuardService ], component: PhotographerDashboardComponent },
      { path: 'photographer-album-photo-dashboard/:albumId', canActivate: [ PhotographerAuthGuardService ], component: AlbumPhotoDashboardComponent },
      { path: 'adminProfile-dashboard', canActivate: [ AdminAuthGuardService ], component: AdminDashboardComponent },
      { path: 'adminProfile-dashboard/photographersInfo/:photographerId', canActivate: [ AdminAuthGuardService ], component: AdminDashboardPhotographerInfoComponent },
      { path: 'super-adminProfile-dashboard', canActivate: [ SuperAdminAuthGuardService ], component: SuperAdminDashboardComponent },
      { path: 'recruiterProfile-dashboard', canActivate: [ RecruiterAuthGuardService ], component: RecruiterDashboardComponent },
      { path: 'dashboard', component: DashboardComponent },
      { path: 'photographer', component: PhotographerComponent },
      { path: 'upload', component: UploadComponent },
      { path: 'message', component: MessageComponent },
      { path: 'logout', component: LogoutComponent },
      { path: '', redirectTo: 'home', pathMatch: 'full' },
      { path: '**', redirectTo: 'home', pathMatch: 'full' }
    ])
  ],
  providers: [
    FileService,
    PhotographerService,
    PhotoService,
    AlbumService,
    CartService,
    CategoryService,
    UserAuthService,
    AuthGuardService,
    DashboardService,
    ToastrService,
    StripeService,
    PhotographerAuthGuardService,
    AdminService,
    SuperAdminService,
    RecruiterService,
    AdminAuthGuardService,
    SuperAdminAuthGuardService,
    RecruiterAuthGuardService,
    BuyListService,
    CustomService,
    SearchHelperService,
    MessageService,
    PhotographerPaymentService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

