import {Injectable} from "@angular/core";
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from "@angular/router";
import {UserAuthService} from "../user-auth.service";
import {ToastrService} from "../../common/toastr.service";

@Injectable()
export class RecruiterAuthGuardService implements CanActivate {
  constructor(private userAuthService: UserAuthService,
              private router: Router,
              private toastrService: ToastrService) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    return this.checkLoggedIn(state.url);
  }

  checkLoggedIn(url: string): boolean {
    if (this.userAuthService.isRecruiterLoggedIn()) {
      return true;
    } else {
      this.toastrService.info("Please login as recruiter to access this page.")
      this.router.navigate(['/recruiterProfile-login']);
      return false;
    }
  }
}
