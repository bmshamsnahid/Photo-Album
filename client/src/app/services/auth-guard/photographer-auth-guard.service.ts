import {Injectable} from "@angular/core";
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from "@angular/router";
import {UserAuthService} from "../user-auth.service";
import {ToastrService} from "../../common/toastr.service";
import {AdminAuthGuardService} from "./admin-auth-guard.service";

@Injectable()
export class PhotographerAuthGuardService implements CanActivate {
  constructor(private userAuthService: UserAuthService,
              private router: Router,
              private toastrService: ToastrService,
              private adminAuthGuardService: AdminAuthGuardService) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    return this.checkLoggedIn(state.url);
  }

  checkLoggedIn(url: string): boolean {
    if (this.userAuthService.isAdminLoggedIn()) {
      return true;
    } else {
      if (this.userAuthService.isPhotographerLoggedIn()) {
        return true;
      } else {
        // this.toastrService.info("Please login as photographerProfile to access this page.")
        this.router.navigate(['/photographerProfile-login']);
        return false;
      }
    }
    }
}
