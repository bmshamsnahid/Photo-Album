import {Injectable} from "@angular/core";
import { Http, Response, Headers, RequestOptions} from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';
import {Photographer} from "../model/photographer";
import { environment } from "../../environments/environment";
import {User} from "../model/user";

@Injectable()
export class PhotographerService {
  currentUserObj;
  currentUser;
  token;

  constructor(private http: Http) {
    this.currentUserObj = JSON.parse(localStorage.getItem('currentUserObj'));
    if (this.currentUserObj != null) {
      this.currentUser = this.currentUserObj.currentUser;
      this.token = this.currentUserObj.token;
    }
  }

  createPhotographer(photographer: Photographer) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    let options = new RequestOptions({ 'headers': headers });

    return this.http.post(`${environment.baseUrl}/photographer`, JSON.stringify(photographer), options)
      .map((response: Response) => {
        return response.json();
      })
      .catch(this.handleError);
  }

  getPhotographer(photographerId: string) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', this.token);
    const options = new RequestOptions({ headers: headers});

    return this.http.get(`${environment.baseUrl}/photographer/${photographerId}`)
      .map((response: Response) => response.json())
      .catch(this.handleError);
  }

  getAllPhotographer() {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', this.token);
    const options = new RequestOptions({ headers: headers});

    return this.http.get(`${environment.baseUrl}/photographer`)
      .map((response: Response) => response.json())
      .catch(this.handleError);
  }

  updatePhotographer(photographer: Photographer) {
    console.log('Updating a photographer.');
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', this.token);
    const options = new RequestOptions({ headers: headers});

    return this.http.patch(`${environment.baseUrl}/photographer/${photographer._id}`, JSON.stringify(photographer), options)
      .map((response: Response) => response.json())
      .catch(this.handleError);
  }

  private handleError(error?: Response) {
    if (error) {
      console.log('Error in Service: ' + error);
      return Observable.throw(error.json().error || 'Server Error');
    } else {
      console.log('Unknown err');
    }
  }

}
