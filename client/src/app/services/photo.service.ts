import {Injectable} from "@angular/core";
import { Http, Response, Headers, RequestOptions} from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';
import { environment } from "../../environments/environment";
import {Album} from "../model/album";
import {User} from "../model/user";
import {Photo} from "../model/photo";

@Injectable()
export class PhotoService {
  currentUserObj;
  currentUser;
  token;

  constructor(private http: Http) {
    this.currentUserObj = JSON.parse(localStorage.getItem('currentUserObj'));
    if (this.currentUserObj != null) {
      this.currentUser = this.currentUserObj.currentUser;
      this.token = this.currentUserObj.token;
    }
  }

  updatePhoto(photo: Photo) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', this.token);
    const options = new RequestOptions({ headers: headers});

    return this.http.patch(`${environment.baseUrl}/photo/${photo._id}`, JSON.stringify(photo), options)
      .map((response: Response) => response.json())
      .catch(this.handleError);
  }

  getPhoto(photoId: string) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', this.token);
    const options = new RequestOptions({ headers: headers});

    return this.http.get(`${environment.baseUrl}/photo/${photoId}`, options)
      .map((response: Response) => response.json())
      .catch(this.handleError);
  }

  getPublicPhoto() {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', this.token);
    const options = new RequestOptions({ headers: headers});

    return this.http.get(`${environment.baseUrl}/photo/public`, options)
      .map((response: Response) => response.json())
      .catch(this.handleError);
  }

  getSearchPhoto(searchKeyword: string) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', this.token);
    const options = new RequestOptions({ headers: headers});

    return this.http.get(`${environment.baseUrl}/photo/search/${searchKeyword}`, options)
      .map((response: Response) => response.json())
      .catch(this.handleError);
  }

  deletePhoto(photo: Photo) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', this.token);
    const options = new RequestOptions({ headers: headers});

    return this.http.delete(`${environment.baseUrl}/photo/${photo._id}`, options)
      .map((response: Response) => response.json())
      .catch(this.handleError);
  }

  private handleError(error?: Response) {
    if (error) {
      console.log(error);
      return Observable.throw(error.json().error || 'Server Error');
    } else {
      console.log('Unknown err');
    }
  }
}
