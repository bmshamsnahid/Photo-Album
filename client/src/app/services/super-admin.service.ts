import {Injectable} from "@angular/core";
import { Http, Response, Headers, RequestOptions} from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';
import {Photographer} from "../model/photographer";
import { environment } from "../../environments/environment";
import {Admin} from "../model/admin";
import {SuperAdmin} from "../model/superAdmin";

@Injectable()
export class SuperAdminService {
  constructor(private http: Http) {}

  createSuperAdmin(superAdmin: SuperAdmin) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    let options = new RequestOptions({ 'headers': headers });

    return this.http.post(`${environment.baseUrl}/super-admin`, JSON.stringify(superAdmin), options)
      .map((response: Response) => {
        return response.json();
      })
      .catch(this.handleError);
  }

  private handleError(error?: Response) {
    if (error) {
      console.log('Error in Service: ' + error);
      return Observable.throw(error.json().error || 'Server Error');
    } else {
      console.log('Unknown err');
    }
  }

}
