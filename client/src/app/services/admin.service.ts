import {Injectable} from "@angular/core";
import { Http, Response, Headers, RequestOptions} from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';
import {Photographer} from "../model/photographer";
import { environment } from "../../environments/environment";
import {Admin} from "../model/admin";

@Injectable()
export class AdminService {
  constructor(private http: Http) {}

  createAdmin(admin: Admin) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    let options = new RequestOptions({ 'headers': headers });

    console.log('Creating the admin: ');
    console.log(admin);

    return this.http.post(`${environment.baseUrl}/admin`, JSON.stringify(admin), options)
      .map((response: Response) => {
        return response.json();
      })
      .catch(this.handleError);
  }

  private handleError(error?: Response) {
    if (error) {
      console.log('Error in Service: ' + error);
      return Observable.throw(error.json().error || 'Server Error');
    } else {
      console.log('Unknown err');
    }
  }

}
