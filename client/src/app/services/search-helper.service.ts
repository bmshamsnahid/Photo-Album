import {Injectable} from "@angular/core";
import {Router} from "@angular/router";
import {Location} from "@angular/common";

@Injectable()
export class SearchHelperService {
  constructor(private router: Router,
              private location: Location) {}
  photoSearch(searchKeyword: string) {
    location.reload();
    this.router.navigate([`search-photo-keyword/${searchKeyword}`]);
  }
  albumSearch(searchKeyword: string) {
    location.reload();
    this.router.navigate([`search-album-keyword/${searchKeyword}`]);
  }
  photographerSearch(searchKeyword:string) {
    location.reload();
    this.router.navigate([`search-photographer-keyword/${searchKeyword}`]);
  }
  recruitersearch(searchKeyword: string) {
    location.reload();
    this.router.navigate([`search-recruiter-keyword/${searchKeyword}`]);
  }
}
