import {Injectable} from "@angular/core";
import { Http, Response, Headers, RequestOptions} from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';
import { environment } from "../../environments/environment";
import {Category} from "../model/category";

@Injectable()
export class CategoryService {
  currentUserObj;
  currentUser;
  token;

  constructor(private http: Http) {
    this.currentUserObj = JSON.parse(localStorage.getItem('currentUserObj'));
    if (this.currentUserObj != null) {
      this.currentUser = this.currentUserObj.currentUser;
      this.token = this.currentUserObj.token;
    }
  }

  getCategory() {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    let options = new RequestOptions({ 'headers': headers });

    return this.http.get(`${environment.baseUrl}/category`, options)
      .map((response: Response) => {
        return response.json();
      })
      .catch(this.handleError);
  }

  createCategory(category: Category) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', this.token);
    const options = new RequestOptions({headers: headers});

    return this.http.post(`${environment.baseUrl}/category`, JSON.stringify(category), options)
      .map((response: Response) => {
        return response.json();
      });
  }

  private handleError(error?: Response) {
    if (error) {
      console.log('Error in Service: ' + error);
      return Observable.throw(error.json().error || 'Server Error');
    } else {
      console.log('Unknown err');
    }
  }

}
