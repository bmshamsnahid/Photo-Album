import {Injectable} from "@angular/core";
import { Http, Response, Headers, RequestOptions} from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';
import { environment } from "../../environments/environment";
import {Album} from "../model/album";
import {User} from "../model/user";
import {PhotographerService} from "./photographer.service";
import {PhotoService} from "./photo.service";
import {ToastrService} from "../common/toastr.service";

@Injectable()
export class CartService {
  currentUserObj;
  currentUser;
  token;

  constructor(private http: Http,
              private photographerService: PhotographerService,
              private photoService: PhotoService,
              private toastrService: ToastrService) {
    this.currentUserObj = JSON.parse(localStorage.getItem('currentUserObj'));
    if (this.currentUserObj != null) {
      this.currentUser = this.currentUserObj.currentUser;
      console.log(this.currentUser);
      this.token = this.currentUserObj.token;
    }
  }

  getCart(photographerId: string) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', this.token);
    const options = new RequestOptions({ headers: headers});

    return this.http.get(`${environment.baseUrl}/cart/${photographerId}`)
      .map((response: Response) => response.json())
      .catch(this.handleError);
  }

  removePhoto(photographerId: string, photoId: string) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', this.token);
    const options = new RequestOptions({ headers: headers});

    return this.http.get(`${environment.baseUrl}/cart/wishlist/${photographerId}/${photoId}`)
      .map((response: Response) => response.json())
      .catch(this.handleError);
  }

  addPhotoToBuyList(photographerId: string, photoId: string) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', this.token);
    const options = new RequestOptions({ headers: headers});

    return this.http.get(`${environment.baseUrl}/cart/buyList/${photographerId}/${photoId}`)
      .map((response: Response) => response.json())
      .catch(this.handleError);
  }

  private handleError(error?: Response) {
    if (error) {
      console.log(error);
      return Observable.throw(error.json().error || 'Server Error');
    } else {
      console.log('Unknown err');
    }
  }
}
