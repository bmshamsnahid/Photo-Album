import {Injectable} from "@angular/core";
import { Http, Response, Headers, RequestOptions} from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';
import { environment } from "../../environments/environment";
import {Album} from "../model/album";
import {User} from "../model/user";

@Injectable()
export class AlbumService {
  currentUserObj;
  currentUser;
  token;

  constructor(private http: Http) {
    this.currentUserObj = JSON.parse(localStorage.getItem('currentUserObj'));
    if (this.currentUserObj != null) {
      this.currentUser = this.currentUserObj.currentUser;
      this.token = this.currentUserObj.token;
    }
  }

  getAlbumPhoto(albumId: string): any {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', this.token);
    const options = new RequestOptions({ headers: headers});

    return this.http.get(`${environment.baseUrl}/photo/album/${albumId}`)
      .map((response: Response) => response.json())
      .catch(this.handleError);
  }

  getCategorizedAlbum(categoryId: string) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', this.token);
    const options = new RequestOptions({ headers: headers});

    return this.http.get(`${environment.baseUrl}/album/category/${categoryId}`, options)
      .map((response: Response) => {
        return response.json();
      });
  }

  getPhotographerAlbum(photographerId: string) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', this.token);
    const options = new RequestOptions({ headers: headers});

    return this.http.get(`${environment.baseUrl}/album/photographer/${photographerId}`, options)
      .map((response: Response) => {
        return response.json();
      });
  };

  createAlbum(album: Album) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', this.token);
    const options = new RequestOptions({headers: headers});

    return this.http.post(`${environment.baseUrl}/album`, JSON.stringify(album), options)
      .map((response: Response) => {
        return response.json();
      });
  }

  updateAlbum(album: Album) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', this.token);
    const options = new RequestOptions({headers: headers});

    return this.http.patch(`${environment.baseUrl}/album/${album._id}`, JSON.stringify(album), options)
      .map((response: Response) => {
        return response.json();
      });
  }

  getPublicAlbumsInfo() {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', this.token);
    const options = new RequestOptions({headers: headers});

    return this.http.get(`${environment.baseUrl}/album/publicAlbumsInfo`, options)
      .map((response: Response) => {
        return response.json();
      })
      .catch(this.handleError);
  }

  getPhotographerPublicAlbumsInfo(photographerId) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', this.token);
    const options = new RequestOptions({headers: headers});

    return this.http.get(`${environment.baseUrl}/album/photographersPublicAlbumsInfo/${photographerId}`, options)
      .map((response: Response) => {
        return response.json();
      })
      .catch(this.handleError);
  }

  private handleError(error?: Response) {
    if (error) {
      console.log(error);
      return Observable.throw(error.json().error || 'Server Error');
    } else {
      console.log('Unknown err');
    }
  }
}
