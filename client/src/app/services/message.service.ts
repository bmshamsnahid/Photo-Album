import {Injectable} from "@angular/core";
import { Http, Response, Headers, RequestOptions} from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';
import { environment } from "../../environments/environment";
import {Message} from "../model/message";

@Injectable()
export class MessageService {
  currentUserObj;
  currentUser;
  token;

  unreadMessage: number;

  constructor(private http: Http) {
    this.currentUserObj = JSON.parse(localStorage.getItem('currentUserObj'));
    if (this.currentUserObj != null) {
      this.currentUser = this.currentUserObj.currentUser;
      this.token = this.currentUserObj.token;
    }
  }

  sendMessage(message: Message) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', this.token);
    const options = new RequestOptions({ headers: headers});

    return this.http.post(`${environment.baseUrl}/message`, JSON.stringify(message), options)
      .map((response: Response) => response.json())
      .catch(this.handleError);
  }

  allPhotographerMessage(photographerId: string) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', this.token);
    const options = new RequestOptions({ headers: headers});

    return this.http.get(`${environment.baseUrl}/message/photographer/all/${photographerId}`, options)
      .map((response: Response) => response.json())
      .catch(this.handleError);
  }

  allPhotographerReadMessage(photographerId: string) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', this.token);
    const options = new RequestOptions({ headers: headers});

    return this.http.get(`${environment.baseUrl}/message/photographer/read/${photographerId}`, options)
      .map((response: Response) => response.json())
      .catch(this.handleError);
  }

  allPhotographerUnReadMessage(photographerId: string) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', this.token);
    const options = new RequestOptions({ headers: headers});

    return this.http.get(`${environment.baseUrl}/message/photographer/unread/${photographerId}`, options)
      .map((response: Response) => response.json())
      .catch(this.handleError);
  }

  updateMessage(message: Message) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', this.token);
    const options = new RequestOptions({ headers: headers});

    return this.http.patch(`${environment.baseUrl}/message/${message._id}`, JSON.stringify(message), options)
      .map((response: Response) => response.json())
      .catch(this.handleError);
  }

  private handleError(error?: Response) {
    if (error) {
      console.log(error);
      return Observable.throw(error.json().error || 'Server Error');
    } else {
      console.log('Unknown err');
    }
  }
}
