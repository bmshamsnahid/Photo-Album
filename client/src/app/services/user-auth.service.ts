import {Injectable} from "@angular/core";
import { Http, Response, Headers, RequestOptions} from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';
import {Photographer} from "../model/photographer";
import { environment } from "../../environments/environment";
import {User} from "../model/user";
import {ToastrService} from "../common/toastr.service";

@Injectable()
export class UserAuthService {
  private currentUser: any;
  constructor(private http: Http,
              private toastrService: ToastrService) {}

  isPhotographerLoggedIn(): boolean {
    try {
      let currentUserObj = JSON.parse(localStorage.getItem('currentUserObj'));
      let currentUser = currentUserObj.currentUser;

      if (currentUser) {
        console.log();
        this.currentUser = currentUser;
        if (this.currentUser.isPhotographer == true) {
          return true;
        } else {
          // this.toastrService.warning('Logged in as a photographerProfile to get the resources.');
          return false;
        }
      }
    } catch (e) {
      return false;
    }
    return false;
  }

  isAdminLoggedIn(): boolean {
    console.log('Checking the adminProfile auth status');
    try {
      let currentUserObj = JSON.parse(localStorage.getItem('currentUserObj'));
      let currentUser = currentUserObj.currentUser;

      if (currentUser) {
        console.log();
        this.currentUser = currentUser;
        if (this.currentUser.isAdmin == true) {
          return true;
        } else {
          // this.toastrService.warning('Logged in as a Admin to get the resources.');
          return false;
        }
      }
    } catch (e) {
      return false;
    }
    return false;
  }

  isSuperAdminLoggedIn(): boolean {
    try {
      let currentUserObj = JSON.parse(localStorage.getItem('currentUserObj'));
      let currentUser = currentUserObj.currentUser;

      if (currentUser) {
        console.log();
        this.currentUser = currentUser;
        if (this.currentUser.isSuperAdmin == true) {
          return true;
        } else {
          this.toastrService.warning('Logged in as a Super Admin to get the resources.');
          return false;
        }
      }
    } catch (e) {
      return false;
    }
    return false;
  }

  isRecruiterLoggedIn(): boolean {
    try {
      let currentUserObj = JSON.parse(localStorage.getItem('currentUserObj'));
      let currentUser = currentUserObj.currentUser;

      if (currentUser) {
        console.log();
        this.currentUser = currentUser;
        if (this.currentUser.isRecruiter == true) {
          return true;
        } else {
          // this.toastrService.warning('Logged in as a Recruiter to get the resources.');
          return false;
        }
      }
    } catch (e) {
      return false;
    }
    return false;
  }

  userLogin(user: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    let options = new RequestOptions({ headers: headers });

    user.clientId = environment.clientId;
    user.clientSecret = environment.clientSecret;

    return this.http.post(`${environment.baseUrl}/auth/login`, JSON.stringify(user), options)
      .map((response: Response) => {
        if (response.json().success) {
          let userObj: any = {};
          userObj.currentUser = response.json().data;
          userObj.token = response.json().token;
          localStorage.setItem('currentUserObj', JSON.stringify(userObj));
          return response.json();
        } else if (response.json().message) {
          this.toastrService.warning(response.json().message);
        } else {
          this.toastrService.error('Error in authenticating the user');
        }

      });
  }

  logout(): void {
    this.currentUser = null;
    localStorage.removeItem('currentUserObj');
  }
}
