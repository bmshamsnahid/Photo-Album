import {Component, OnInit} from "@angular/core";
import {Category} from "../../model/category";
import {CategoryService} from "../../services/category.service";
import {ToastrService} from "../../common/toastr.service";
import {Album} from "../../model/album";
import {AlbumService} from "../../services/album.service";
import {Router} from "@angular/router";
import {PhotographerService} from "../../services/photographer.service";
import {Photo} from "../../model/photo";
import {Photographer} from "../../model/photographer";

@Component({
  templateUrl: './album-creator.component.html',
  styleUrls: [
    './album-creator.component.css'
  ]
})
export class AlbumCreatorComponent implements OnInit {

  currentUserObj;
  photographerId;
  photographer: Photographer;
  token;

  name: string;
  description: string;
  selectedCategory: Category;
  categories: Category[];
  isPrivate: boolean;
  album: Album;

  constructor(private categoryService: CategoryService,
              private toastrService: ToastrService,
              private albumService: AlbumService,
              private photographerService: PhotographerService,
              private router: Router) {
    this.photographer = new Photographer();
    this.currentUserObj = JSON.parse(localStorage.getItem('currentUserObj'));
    if (this.currentUserObj != null) {
      console.log(this.currentUserObj);
      this.photographerId = this.currentUserObj.currentUser.photographerId;
      this.photographerService.getPhotographer(this.photographerId)
        .subscribe((response) => {
          if (response.success) {
            this.photographer = response.data;
            console.log(this.photographer);
          } else if (response.message) {
            this.toastrService.warning(response.message);
          } else {
            this.toastrService.error('Fatal server error.');
          }
        });
    }

  }
  ngOnInit() {
    this.categoryService.getCategory().subscribe((response) => {
      if (response.success) {
        this.categories = response.data;
        this.selectedCategory = this.categories[0] || null;
      } else if (response.message) {
        this.toastrService.warning(response.message);
      } else {
        this.toastrService.error('Fatal server error in getting the categories');
      }
    });
  }

  onChange(event) {
    console.log(this.selectedCategory);
  }

  onSelect(event, index) {
    console.log(this.selectedCategory);
  }

  setAlbumPrivacy(value: boolean) {
    if (value == false) {
      this.isPrivate = false;
    } else {
      this.isPrivate = true;
    }
  }


  onSubmitCreateAlbum() {
    if (!this.name) {
      this.toastrService.warning('Please provide the album name.');
    } else if (!this.description) {
      this.toastrService.warning('Please provide the album description.');
    } else if (!this.selectedCategory) {
      this.toastrService.warning('Please provide the album category.');
    } else {
      if(typeof  this.isPrivate == 'undefined') {
        this.toastrService.warning('Album privacy is set to public by default.');
        this.isPrivate = false;
      }
      this.album = new Album();
      this.album.name = this.name;
      this.album.description = this.description;
      this.album.isPrivate = this.isPrivate;
      this.album.category = this.selectedCategory._id;
      this.album.ownerId = this.photographerId;
      this.albumService.createAlbum(this.album)
        .subscribe((response) => {
          if (response.success) {
            this.toastrService.success('Successfully created the album');
            console.log(this.album);
            this.router.navigate([`/photo-upload/${response.data._id}`]);
          } else if (response.message) {
            this.toastrService.warning(response.message);
          } else {
            this.toastrService.error('Fatal server error');
          }
        });
    }
  }

}
