import {Component, OnInit} from "@angular/core";
import {PhotoService} from "../../services/photo.service";
import {ToastrService} from "../../common/toastr.service";
import {Photo} from "../../model/photo";
import {environment} from "../../../environments/environment";

@Component({
  templateUrl: './photos-display.component.html',
  styleUrls: [
    './photos-display.component.css'
  ]
})
export class PhotosDisplayComponent implements OnInit {

  photos: Photo[];
  baseUrl: string = environment.baseUrl;
  p: number = 1;

  constructor(private photoService: PhotoService,
              private toastrService: ToastrService) {}
  ngOnInit() {
    this.photoService.getPublicPhoto()
      .subscribe((response) => {
        if (response.success) {
          this.photos = response.data;
          console.log(this.photos);
        } else if (response.message) {
          this.toastrService.warning(response.message);
        } else {
          this.toastrService.error('Fatal server error');
        }
      });
  }
  onClicked(photo) {

  }
}
