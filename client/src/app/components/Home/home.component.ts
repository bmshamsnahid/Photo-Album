import {Component, OnInit} from "@angular/core";
import { Lightbox, LightboxConfig, LightboxEvent, LIGHTBOX_EVENT, IEvent, IAlbum } from 'angular2-lightbox';
import { Subscription } from 'rxjs/Subscription';
import {CategoryService} from "../../services/category.service";
import {Category} from "../../model/category";
import {ToastrService} from "../../common/toastr.service";
import {CustomService} from "../../services/custom.service";

@Component({
  templateUrl: './home.component.html',
  styleUrls: [
    'home.component.css'
  ]
})
export class HomeComponent implements OnInit {

  propertiesLength: any = {
    albumsLength: 0,
    categoriesLength: 0,
    photographersLength: 0,
    photosLength: 0
  };

  constructor(private categoryService: CategoryService,
              private toastrService: ToastrService,
              private customService: CustomService) {}
  ngOnInit() {
    this.customService.getPropertiesLength()
      .subscribe((response) => {
        if (response.success) {
          this.propertiesLength = response.data;
          console.log(this.propertiesLength);
        } else if (response.message) {
          this.toastrService.warning(response.message);
        } else {
          this.toastrService.error('Fatal server error.');
        }
      });
  }
}
