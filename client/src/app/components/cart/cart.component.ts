import {Component, OnInit, AfterViewInit, OnDestroy, ViewChild, ElementRef, ChangeDetectorRef} from "@angular/core";
import {Photographer} from "../../model/photographer";
import {PhotographerService} from "../../services/photographer.service";
import {ToastrService} from "../../common/toastr.service";
import { NgForm } from '@angular/forms';
import {StripeService} from "../../common/stripe.service";
import {PhotoService} from "../../services/photo.service";
import {Photo} from "../../model/photo";
import {CartService} from "../../services/cart.service";
import {environment} from "../../../environments/environment";
import { Location } from '@angular/common';
import {PhotographerPaymentService} from "../../common/photographer-payment.service";


@Component({
  templateUrl: 'cart.component.html',
  styleUrls: [
    'cart.component.css'
  ]
})
export class CartComponent implements OnInit, AfterViewInit, OnDestroy {
  currentUserObj;
  currentUser;
  token: string;

  photographerId: string;
  photographer: Photographer;

  photos: Photo[];
  wishLists: string[];
  baseUrl: string = environment.baseUrl;
  selectedPhoto: Photo;

  @ViewChild('cardInfo') cardInfo: ElementRef;
  card: any;
  cardHandler = this.onChange.bind(this);
  error: string;

  constructor(private photographerService: PhotographerService,
              private toastrService: ToastrService,
              private photoService: PhotoService,
              private cartService: CartService,
              private cd: ChangeDetectorRef,
              private stripeService: StripeService,
              private location: Location,
              private photographerPaymentService: PhotographerPaymentService) {
    this.photos = [];
    this.selectedPhoto = new Photo();
    this.currentUserObj = JSON.parse(localStorage.getItem('currentUserObj'));
    if (this.currentUserObj != null) {
      this.currentUser = this.currentUserObj.currentUser;
      this.photographerId = this.currentUser.photographerId;
      this.token = this.currentUserObj.token;
    }
  }

  ngOnInit() {
    this.cartService.getCart(this.photographerId)
      .subscribe((response) => {
        if (response.success) {
          this.photos = response.data;
          console.log(response.data);
        } else if (response.message) {
          this.toastrService.warning(response.message);
        } else {
          this.toastrService.error('Fatal server error.');
        }
      });
  }

  onClickRemovePhoto(photo: Photo) {
    this.cartService.removePhoto(this.photographerId, photo._id)
      .subscribe((response) => {
        if (response.success) {
          this.toastrService.success('Successfully remove the photo.');
          location.reload();
        } else if (response.message) {
          this.toastrService.warning(response.message);
        } else {
          this.toastrService.error('Fatal server error');
        }
      });
  }

  onClickDownloadPhoto(photo) {
    this.selectedPhoto = photo;
  }

  ngAfterViewInit() {
    this.card = elements.create('card');
    this.card.mount(this.cardInfo.nativeElement);

    this.card.addEventListener('change', this.cardHandler);
  }

  ngOnDestroy() {
    this.card.removeEventListener('change', this.cardHandler);
    this.card.destroy();
  }

  onChange({error}) {
    if (error) {
      this.error = error.message;
    } else {
      this.error = null;
    }
    this.cd.detectChanges();
  }

  async onSubmit(form: NgForm) {
    const {token, error} = await stripe.createToken(this.card);

    if (error) {
      console.log('Something is wrong:', error);
    } else {
      console.log('Success!', token);
      this.stripeService.confirmPayment('test@mymail.com', token)
        .subscribe((response) => {
          if (response._body) {
            this.toastrService.success('Payment success');
            this.cartService.addPhotoToBuyList(this.photographerId, this.selectedPhoto._id)
              .subscribe((response) => {
                if (response.success) {
                  this.toastrService.success('Added to your buyList.');
                  this.photographerPaymentService.payPhotoOwner(this.selectedPhoto)
                    .subscribe((response) => {
                      if (response.success) {
                        this.downloadPhoto(() => {
                          location.reload();
                        });
                      } else if (response.message) {
                        this.toastrService.warning(response.message);
                      } else {
                          this.toastrService.error('Fatal server errro.');
                      }
                    });
                } else if (response.message) {
                  this.toastrService.warning(response.message);
                } else {
                  this.toastrService.error('Fatal server error.');
                }
              });
          } else {
            this.toastrService.error('Error in payment');
          }
        });
      // ...send the token to the your backend to process the charge
    }

  }

  downloadPhoto(cb) {
    window.open(this.baseUrl + this.selectedPhoto.originalPath, "_blank");
    return cb();
  }
}
