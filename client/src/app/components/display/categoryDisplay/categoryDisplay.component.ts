import {Component, OnInit} from "@angular/core";
import {CategoryService} from "../../../services/category.service";
import {ToastrService} from "../../../common/toastr.service";
import {Category} from "../../../model/category";

@Component({
  templateUrl: './categoryDisplay.component.html',
  styleUrls: [
    './categoryDisplay.component.css'
  ]
})
export class CategoryDisplayComponent implements OnInit {
  categories: Category[];
  constructor(private categoryService: CategoryService,
              private toastrService: ToastrService) {}
  ngOnInit() {
    this.categoryService.getCategory()
      .subscribe((response) => {
        if (response.success) {
          this.categories = response.data;
          console.log(this.categories);
        } else if (response.message) {
          this.toastrService.warning(response.message);
        } else {
          this.toastrService.error('Fatal server error.');
        }
      });
  }

}
