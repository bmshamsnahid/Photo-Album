import {AfterViewInit, Component, OnInit} from "@angular/core";
import {AlbumService} from "../../../services/album.service";
import {ToastrService} from "../../../common/toastr.service";
import {Album} from "../../../model/album";
import {ActivatedRoute} from "@angular/router";
import {Photographer} from "../../../model/photographer";
import {PhotographerService} from "../../../services/photographer.service";

@Component({
  templateUrl: './categoryAlbumDisplay.component.html',
  styleUrls: [
    './categoryAlbumDisplay.component.css'
  ]
})
export class CategoryAlbumDisplayComponent implements OnInit, AfterViewInit {
  ngAfterViewInit(): void {
    this.categoryId = this.activatedRoute.snapshot.params['categoryId'];
    this.albumService.getCategorizedAlbum(this.categoryId)
      .subscribe((response) => {
        if (response.success) {
          this.albums = response.data;

          for (let index=0; index<this.albums.length; index++) {
            let album = this.albums[index];
            this.photographerService.getPhotographer(album.ownerId)
              .subscribe((response) => {
                if (response.success) {
                  album.owner = response.data;
                  this.isDataLoaded = true;
                }
              });
            if (index == this.albums.length) {

            }
          }
        } else if (response.message) {
          this.toastrService.warning(response.message);
        } else {
          this.toastrService.error('Fatal server error.');
        }
      });
  }

  albums: Album[];
  categoryId: string;
  owner: Photographer;
  isDataLoaded: boolean = false;
  constructor (private albumService: AlbumService,
               private toastrService: ToastrService,
               private activatedRoute: ActivatedRoute,
               private photographerService: PhotographerService) {
  }
  ngOnInit() {}

  onClickAlbum(album: Album) {
    console.log('Clicked album.');
    console.log(album);
  }
}
