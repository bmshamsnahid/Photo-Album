import {Component, OnInit} from "@angular/core";
import {Photographer} from "../../../model/photographer";
import {AlbumService} from "../../../services/album.service";
import {ToastrService} from "../../../common/toastr.service";
import {Album} from "../../../model/album";
import {AlbumInfo} from "../../../model/albumInfo";
import {Category} from "../../../model/category";
import {CategoryService} from "../../../services/category.service";
import {Location} from '@angular/common';
import {ActivatedRoute, Router} from "@angular/router";
import {PhotographerService} from "../../../services/photographer.service";

@Component({
  templateUrl: './photographer-albums.component.html',
  styleUrls: ['./photographer-albums.component.css']
})
export class PhotographerAlbumsComponent implements OnInit {
  albums: Album[];
  albumsInfo: AlbumInfo[];
  categoryId: string;
  owner: Photographer;
  isDataLoaded: boolean = false;
  photographerId: string;
  constructor (private albumService: AlbumService,
               private toastrService: ToastrService,
               private activatedRoute: ActivatedRoute,
               private photographerService: PhotographerService) {
  }
  ngOnInit() {
    this.photographerId = this.activatedRoute.snapshot.params['photographerId'];
    this.albumService.getPhotographerPublicAlbumsInfo(this.photographerId)
      .subscribe((response) => {
        if (response.success) {
          this.albumsInfo = response.data;
          console.log(this.albumsInfo);
        } else if (response.message) {
          this.toastrService.warning(response.message);
        } else {
          this.toastrService.error('Fatal server error');
        }
      });
  }

  onClickAlbum(album: Album) {
    console.log('Clicked album.');
    console.log(album);
  }
}
