import {Component, OnInit} from "@angular/core";
import {BuyListService} from "../../services/buyList.service";
import {Photo} from "../../model/photo";
import {ToastrService} from "../../common/toastr.service";
import {environment} from "../../../environments/environment";

@Component({
  templateUrl: './buy-list.component.html',
  styleUrls: [
    './buy-list.component.css'
  ]
})
export class BuyListComponent implements OnInit {
  currentUserObj;
  currentUser;
  token: string;

  photographerId: string;
  photos: Photo[];
  baseUrl: string = environment.baseUrl;

  constructor(private buyListService: BuyListService,
              private toastrService: ToastrService) {
    this.currentUserObj = JSON.parse(localStorage.getItem('currentUserObj'));
    if (this.currentUserObj != null) {
      this.currentUser = this.currentUserObj.currentUser;
      this.photographerId = this.currentUser.photographerId;
      this.token = this.currentUserObj.token;
    }
  }

  ngOnInit() {
    this.buyListService.getBuyList(this.photographerId)
      .subscribe((response) => {
        if (response.success) {
          this.photos = response.data;
        } else if (response.message) {
          this.toastrService.warning(response.message);
        } else {
          this.toastrService.error('Fatal Server Error.');
        }
      });
  }

  onClickDownloadPhoto(photo: Photo) {
    window.open(this.baseUrl + photo.originalPath, "_blank");
  }
}
