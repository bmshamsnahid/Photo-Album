import {Component, OnInit} from "@angular/core";
import {UserAuthService} from "../../services/user-auth.service";
import {User} from "../../model/user";
import {PhotographerService} from "../../services/photographer.service";
import {CategoryService} from "../../services/category.service";
import {PhotoService} from "../../services/photo.service";
import {AlbumService} from "../../services/album.service";
import {Router} from "@angular/router";
import {ToastrService} from "../../common/toastr.service";
import {SearchHelperService} from "../../services/search-helper.service";

@Component({
  selector: 'album-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: [
    'navbar.component.css'
  ]
})
export class NavbarComponent implements OnInit {

  currentUserObj;
  currentUser;
  token;
  photographerId: string;

  photoSearchKeyword: string;
  photographerSearchKeyword: string;
  albumSearchKeyword: string;
  recruiterSearchKeyword: string;

  constructor(public userAuthService: UserAuthService,
              private categoryService: CategoryService,
              private photographerService: PhotographerService,
              private photoService: PhotoService,
              private albumService: AlbumService,
              private router: Router,
              private toastrService: ToastrService,
              private searchHelperService: SearchHelperService) {}
  ngOnInit() {
    this.currentUserObj = JSON.parse(localStorage.getItem('currentUserObj'));
    if (this.currentUserObj != null) {
      this.currentUser = this.currentUserObj.currentUser;
      this.photographerId = this.currentUser.photographerId;
      this.token = this.currentUserObj.token;
    }
  }

  onClickPhotographerProfile() {
    this.router.navigate([`photographer-profile/${this.photographerId}`]);
  }

  onClickPhotoSearch() {
    if (typeof this.photoSearchKeyword == 'undefined' || this.photoSearchKeyword.length <= 0) {
      this.toastrService.warning('Invalid or incomplete photo search keyword.');
    } else {
      this.searchHelperService.photoSearch(this.photoSearchKeyword);
      // this.router.navigate([`search-photo-keyword/${this.photoSearchKeyword}`]);
    }
  }

  onClickAlbumSearch() {
    if (typeof this.albumSearchKeyword == 'undefined' || this.albumSearchKeyword.length <= 0) {
      this.toastrService.warning('Invalid or incomplete album search keyword.');
    } else {
      this.router.navigate([`search-album-keyword/${this.albumSearchKeyword}`]);
    }
  }

  onClickPhotographerSearch() {
    if (typeof this.photographerSearchKeyword == 'undefined' || this.photographerSearchKeyword.length <= 0) {
      this.toastrService.warning('Invalid or incomplete photographer search keyword.');
    } else {
      this.router.navigate([`search-photographer-keyword/${this.photographerSearchKeyword}`]);
    }
  }

  onClickRecruiterSearch() {
    if (typeof this.recruiterSearchKeyword == 'undefined' || this.recruiterSearchKeyword.length <= 0) {
      this.toastrService.warning('Invalid or incomplete recruiter search keyword.');
    } else {
      this.router.navigate([`search-recruiter-keyword/${this.recruiterSearchKeyword}`]);
    }
  }
}
