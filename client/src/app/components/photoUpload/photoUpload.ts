import {Component, OnInit} from "@angular/core";
import { FileSelectDirective, FileUploader} from 'ng2-file-upload';
import {saveAs} from 'file-saver';
import { Http, Response, Headers, RequestOptions} from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';
import {FileService} from "../../fiel-service";
import {environment} from "../../../environments/environment";
import {CategoryService} from "../../services/category.service";
import {ToastrService} from "../../common/toastr.service";
import {FormControl, FormGroupDirective, NgForm, Validators} from '@angular/forms';
import {Category} from "../../model/category";
import {ActivatedRoute, Router} from "@angular/router";
import 'rxjs/add/operator/switchMap';

// const uri = `${environment.baseUrl}/photo/upload/123`;

@Component({
  templateUrl: './photoUpload.html',
  styleUrls: [
    './photoUpload.css'
  ]
})
export class PhotoUpload implements OnInit{
  // uploader:FileUploader = new FileUploader({url: uri});
  uploader: FileUploader;
  uri: string;

  attachmentList:any = [];

  constructor(private activatedRoute: ActivatedRoute){

    let id = this.activatedRoute.snapshot.paramMap.get('id');
    const uri = `${environment.baseUrl}/photo/upload/${id}`;
    this.uploader = new FileUploader({url: uri});

    this.uploader.onAfterAddingFile = (file)=> { file.withCredentials = false; };

    this.uploader.onCompleteItem = (item:any, response:any , status:any, headers:any) => {
      this.attachmentList.push(JSON.parse(response));
      console.log('Under the onCompleteItem');
      console.log(JSON.parse(response));
    }
  }

  ngOnInit() {
    let id = this.activatedRoute.snapshot.paramMap.get('id');
  }

  completeFileUploadOperation() {
    console.log('On complete the file upload operation.');
    console.log(this.attachmentList);
  }

}
