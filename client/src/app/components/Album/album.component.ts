import {Component, OnInit, AfterViewInit, OnDestroy, ViewChild, ElementRef, ChangeDetectorRef} from "@angular/core";
import {AlbumService} from "../../services/album.service";
import {Photo} from "../../model/photo";
import {ToastrService} from "../../common/toastr.service";
import { environment } from "../../../environments/environment";
import { NgForm } from '@angular/forms';
import {StripeService} from "../../common/stripe.service";
import {PhotoService} from "../../services/photo.service";
import {ActivatedRoute} from "@angular/router";
import {Album} from "../../model/album";
import {UserAuthService} from "../../services/user-auth.service";
import {PhotographerService} from "../../services/photographer.service";
import {Photographer} from "../../model/photographer";
import {CartService} from "../../services/cart.service";

@Component({
  templateUrl: './album.component.html',
  styleUrls: [
    'album.component.css'
  ]
})
export class AlbumComponent implements OnInit, AfterViewInit, OnDestroy {

  album: Album;
  photoes: Photo[];
  selectedPhoto: Photo;
  selectedPhotoIndex: number;
  baseUrl: string = environment.baseUrl;

  voteAvailability: boolean = false;

  currentUserObj;
  currentUser;
  token;

  photographerId: string;
  photographer: Photographer;

  @ViewChild('cardInfo') cardInfo: ElementRef;
  card: any;
  cardHandler = this.onChange.bind(this);
  error: string;

  constructor(private albumService: AlbumService,
              private toastrService: ToastrService,
              private photoService: PhotoService,
              private activatedRoute: ActivatedRoute,
              private cd: ChangeDetectorRef,
              private stripeService: StripeService,
              public userAuthService: UserAuthService,
              private photographerService: PhotographerService,
              private cartService: CartService) {
    this.selectedPhoto = new Photo();
    this.photographer = new Photographer();
    this.currentUserObj = JSON.parse(localStorage.getItem('currentUserObj'));
    if (this.currentUserObj != null) {
      console.log(this.currentUserObj);
      this.currentUser = this.currentUserObj.currentUser;
      if (this.currentUser.isPhotographer) {
        this.photographerId = this.currentUser.photographerId;
      }
      this.token = this.currentUserObj.token;
    }
  }
  ngOnInit() {
    let albumId = this.activatedRoute.snapshot.params['id'];
    this.albumService.getAlbumPhoto(albumId)
      .subscribe((response) => {
        if (response.success) {
          this.photoes = response.data;
          this.photoes.forEach((photo) => {
            if (photo.upVoterList.indexOf(this.photographerId) >= 0) {
              photo.voteAvailability = false;
            } else {
              photo.voteAvailability = true;
            }
            return photo;
          });
          this.photoes.forEach((photo) => {
            console.log('Name: ' + photo.name);
            console.log('visibility: ' + photo.voteAvailability);
          });
        } else if (response.message) {
          this.toastrService.warning(response.message);
        } else {
          this.toastrService.error('Fatal server error.');
        }
      });

    this.photographerService.getPhotographer(this.photographerId)
      .subscribe((response) => {
        if (response.success) {
          this.photographer = response.data;
          console.log(this.photographer);
        } else if (response.message) {
          this.toastrService.warning(response.message);
        } else {
          this.toastrService.error('Error in getting the photographer info.');
        }
      });
  }

  onClicked(photo: Photo) {
    this.selectedPhoto = photo;
    this.selectedPhotoIndex = this.photoes.indexOf(this.selectedPhoto);
  }

  onClickPhotoView(photo: Photo) {
    let currentPhotoIndex = this.photoes.indexOf(photo);
    photo.views = photo.views + 1;
    this.photoes[currentPhotoIndex] = photo;
    this.photoService.updatePhoto(photo)
      .subscribe((response) => {
        if (response.success) {

        } else if (response.message) {
          this.toastrService.warning(response.message);
        } else {
          this.toastrService.error('Fatal server error.');
        }
      });
  }

  onClickPhotoLike(photo: Photo) {
    let currentPhotoIndex = this.photoes.indexOf(photo);
    photo.likes = photo.likes + 1;
    this.photoes[currentPhotoIndex] = photo;
    this.photoService.updatePhoto(photo)
      .subscribe((response) => {
        if (response.success) {
          this.toastrService.success('Your likes being counted.');
        } else if (response.message) {
          this.toastrService.warning(response.message);
        } else {
          this.toastrService.error('Fatal server error.');
        }
      });
  }

  onClickPhotoVote(photo: Photo, value: number) {
    let currentPhotoIndex = this.photoes.indexOf(photo);

    if (photo.upVoterList.indexOf(this.photographerId) < 0) {
      photo.upVoterList.push(this.photographerId);
      photo.upVote = photo.upVote + value;
      this.toastrService.success('Your vote being casted.');
    } else {;
      this.toastrService.warning('You have already voted the photo.');
    }

    this.photoes[currentPhotoIndex] = photo;
    this.photoService.updatePhoto(photo)
      .subscribe((response) => {
        if (response.success) {

        } else if (response.message) {
          this.toastrService.warning(response.message);
        } else {
          this.toastrService.error('Fatal server error.');
        }
      });
  }

  onClickAddToCart(photo: Photo) {
    console.log('Add the photo: ');
    console.log(photo);
    console.log('To the user');
    console.log(this.photographer);
    if (this.photographer.wishlists.indexOf(photo._id) < 0) {
      this.photographer.wishlists.push(photo._id);
      this.photographerService.updatePhotographer(this.photographer)
        .subscribe((response) => {
          if (response.success) {
            this.toastrService.success('Successfully added to the cart.')
          } else if (response.message) {
            this.toastrService.warning(response.message);
          } else {
            this.toastrService.error('Fatal server error.');
          }
        });
    } else {
      this.toastrService.warning('Already this photo is added to the cart.');
    }
  }

  ngAfterViewInit() {
    this.card = elements.create('card');
    this.card.mount(this.cardInfo.nativeElement);

    this.card.addEventListener('change', this.cardHandler);
  }

  ngOnDestroy() {
    this.card.removeEventListener('change', this.cardHandler);
    this.card.destroy();
  }

  onChange({ error }) {
    if (error) {
      this.error = error.message;
    } else {
      this.error = null;
    }
    this.cd.detectChanges();
  }

  async onSubmit(form: NgForm) {
    const { token, error } = await stripe.createToken(this.card);

    if (error) {
      console.log('Something is wrong:', error);
    } else {
      console.log('Success!', token);
      this.stripeService.confirmPayment('test@mymail.com', token)
        .subscribe((response) => {

          if (response._body) {
            this.toastrService.success('Payment success');
            this.cartService.addPhotoToBuyList(this.photographerId, this.selectedPhoto._id)
              .subscribe((response) => {
                if (response.success) {
                  this.toastrService.success('Added to your buyList.');
                  this.downloadPhoto(() => {
                    location.reload();
                  });
                } else if (response.message) {
                  this.toastrService.warning(response.message);
                } else {
                  this.toastrService.error('Fatal server error.');
                }
              });
          } else {
            this.toastrService.error('Error in payment');
          }
        });
      // ...send the token to the your backend to process the charge
    }
  }

  buyImage() {
    console.log('Try to buy image');
  }

  downloadPhoto(cb) {
    window.open(this.baseUrl + this.selectedPhoto.originalPath, "_blank");
    return cb();
  }

}
