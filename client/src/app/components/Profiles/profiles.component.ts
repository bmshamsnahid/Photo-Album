import {Component, OnInit} from "@angular/core";
import {PhotographerService} from "../../services/photographer.service";
import {Photographer} from "../../model/photographer";
import {ToastrService} from "../../common/toastr.service";
import {Router} from "@angular/router";
import {Message} from "../../model/message";
import {MessageService} from "../../services/message.service";

@Component({
  templateUrl: './profiles.component.html',
  styleUrls: [
    './profiles.component.css'
  ]
})
export class ProfilesComponent implements OnInit {
  photographers: Photographer[];
  message: Message;
  selectedPhotographer: Photographer;
  constructor(private photographerService: PhotographerService,
              private toastrService: ToastrService,
              private router: Router,
              private messageService: MessageService) {}
  ngOnInit() {
    this.message = new Message();
    this.selectedPhotographer = new Photographer();
    this.photographerService.getAllPhotographer()
      .subscribe((response) => {
          if (response.success) {
            this.photographers = response.data;
            console.log(this.photographers);
          } else if (response.message) {
            this.toastrService.warning(response.message);
          } else {
            this.toastrService.error('Fatal server error');
          }
        }
      );
  }
  onClickBrowseAlbums(photographer: Photographer) {
    this.router.navigate([`/photographersAlbumDisplay/${photographer._id}`]);
  }

  onClickMessageButton(photographer: Photographer) {
    console.log('Message buttom clicked.');
    this.selectedPhotographer = photographer;
  }

  onClickSendMessage() {
    this.message.userId = this.selectedPhotographer._id;
    this.message.isRead = false;
    if (typeof this.message.title == 'undefined' || this.message.title.length <= 0) {
      this.toastrService.warning('Invalid or incomplete message title.');
    } else if (typeof this.message.description == 'undefined' || this.message.description.length <= 0) {
      this.toastrService.warning('Invalid or incomplete messgae description');
    } else if ( typeof this.message.senderName == 'undefined' || this.message.senderName.length <= 0) {
      this.toastrService.warning('Invalid or incomplete name.');
    } else if (typeof this.message.senderPhoneNumber == 'undefined' || this.message.senderPhoneNumber.length <= 10) {
      this.toastrService.warning('Invalid or incomplete phone number');
    } else {
      this.messageService.sendMessage(this.message)
        .subscribe((response) => {
          if (response.success) {
            this.toastrService.success('Successfully message sent.');
            this.message = new Message();
          } else if (response.message) {
            this.toastrService.warning(response.message);
          } else {
            this.toastrService.error('Fatal server error.')
          }
        });
    }
  }
}
