import {AfterViewInit, ChangeDetectorRef, Component, ElementRef, OnDestroy, OnInit, ViewChild} from "@angular/core";
import {ToastrService} from "../../../common/toastr.service";
import {PhotographerService} from "../../../services/photographer.service";
import {ActivatedRoute} from "@angular/router";
import {Photographer} from "../../../model/photographer";
import {StripeService} from "../../../common/stripe.service";
import {NgForm} from "@angular/forms";


@Component({
  templateUrl: './photographer-profile.component.html',
  styleUrls: [
    './photographer-profile.component.css'
  ]
})
export class PhotographerProfileComponent implements OnInit, AfterViewInit, OnDestroy {
  photographerId: string;
  photographer: Photographer;

  @ViewChild('cardInfo') cardInfo: ElementRef;
  card: any;
  cardHandler = this.onChange.bind(this);
  error: string;


  constructor(private toastrService: ToastrService,
              private photographerService: PhotographerService,
              private activateRoute: ActivatedRoute,
              private cd: ChangeDetectorRef,
              private stripeService: StripeService) {
    this.photographer = new Photographer();
  }
  ngOnInit() {
    this.photographerId = this.activateRoute.snapshot.params['photographerId'];
    console.log(this.photographerId);
    this.photographerService.getPhotographer(this.photographerId)
      .subscribe((response) => {
        if (response.success) {
          this.photographer = response.data;
          console.log(this.photographer);
        } else if (response.message) {
          this.toastrService.warning(response.message);
        } else {
          this.toastrService.error('Fatal server error.');
        }
      });
  }

  onClickUpdatePhotographer() {
    console.log('New updated photographer info: ');
    console.log(this.photographer);
    this.photographerService.updatePhotographer(this.photographer)
      .subscribe((response) => {
        if (response.success) {
          this.toastrService.success('Successfully updated the photographer profile.');
          this.photographer = response.data;
          console.log('Updated photographer information.');
          console.log(this.photographer);
        } else if (response.message) {
          this.toastrService.warning(response.message);
        } else {
          this.toastrService.error('Fatal server error.');
        }
      });
  }

  onClickDeletePhotographer() {
    console.log('Deleted photographer info: ');
    console.log(this.photographer);
  }

  onClickBecomingPremiumPhotographer() {
    console.log('Becoming Premium');
  }

  ngAfterViewInit() {
    this.card = elements.create('card');
    this.card.mount(this.cardInfo.nativeElement);

    this.card.addEventListener('change', this.cardHandler);
  }

  ngOnDestroy() {
    this.card.removeEventListener('change', this.cardHandler);
    this.card.destroy();
  }

  onChange({error}) {
    if (error) {
      this.error = error.message;
    } else {
      this.error = null;
    }
    this.cd.detectChanges();
  }

  async onSubmit(form: NgForm) {
    const {token, error} = await stripe.createToken(this.card);

    if (error) {
      console.log('Something is wrong:', error);
    } else {
      console.log('Success!', token);
      this.stripeService.confirmPayment('test@mymail.com', token)
        .subscribe((response) => {
          if (response._body) {
            this.toastrService.success('Payment success');
            this.photographer.isPremium = true;
            this.photographerService.updatePhotographer(this.photographer)
              .subscribe((response) => {
                if (response.success) {
                  this.toastrService.success('Thank you for being a premium member.');
                } else if (response.message) {
                  this.toastrService.warning(response.message);
                  this.photographer.isPremium = false;
                } else {
                  this.toastrService.warning('Fatal Server error');
                  this.photographer.isPremium = false;
                }
              });
          } else {
            this.toastrService.error('Error in payment');
          }
        });
    }
  }
}
