import {Component} from "@angular/core";
import {CategoryService} from "../../services/category.service";
import {Category} from "../../model/category";
import {ToastrService} from "../../common/toastr.service";

@Component({
  templateUrl: './category.component.html',
  styleUrls: [
    './category.component.css'
  ]
})
export class CategoryComponent {
  title: string;
  description: string;
  category: Category;

  constructor(private categoryService: CategoryService,
              private toastrService: ToastrService) {}
  ngOnInit() {}

  onSubmitCreateCategory() {
    console.log('Title: ' + this.title);
    console.log('Description: ' + this.description);
    if (typeof this.title == 'undefined') {
      this.toastrService.warning('Please enter a category title.');
    } else if (typeof this.description  == 'undefined') {
      this.toastrService.warning('Please enter a category description.');
    } else {
      this.category = new Category();
      this.category.title = this.title;
      this.category.description = this.description;
      this.categoryService.createCategory(this.category)
        .subscribe((response) => {
          if (response.success) {
            this.toastrService.success('Successfullly creted the category.');
            console.log(response.data);
            console.log();
          } else if (response.message) {
            this.toastrService.warning(response.message);
          } else {
            this.toastrService.error('Fatal server error.');
          }
        });
    }

  }
}
