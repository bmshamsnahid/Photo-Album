import {Component, OnInit} from "@angular/core";
import {FormGroup, FormControl, Validators, AbstractControl, FormBuilder} from '@angular/forms';
import {Photographer} from "../../../model/photographer";
import {PhotographerService} from "../../../services/photographer.service";
import {ToastrService} from "../../../common/toastr.service";
import {Router} from "@angular/router";
import {Recruiter} from "../../../model/recruiter";
import {RecruiterService} from "../../../services/recruiter.service";

function comparePassword(c: AbstractControl): {[key: string]: boolean} | null {
  let passwordControl = c.get('password');
  let confirmControl = c.get('retypePassword');

  if (passwordControl.pristine || confirmControl.pristine) {
    return null;
  }

  if (passwordControl.value === confirmControl.value) {
    return null;
  }
  return {'mismatchedPassword': true};
}

@Component({
  templateUrl: './recruiter-register.component.html',
  styleUrls: [
    './recruiter-register.component.css'
  ]
})
export class RecruiterRegisterComponent {
  formTitle: string;
  recruiter: Recruiter;
  registerForm: FormGroup;

  name = new FormControl('', [Validators.required]);
  email = new FormControl('', [Validators.email]);
  password = new FormControl('', [Validators.required, Validators.minLength(8),]);
  retypePassword = new FormControl('', [Validators.required]);

  constructor(private formBuilder: FormBuilder,
              private recruiterService: RecruiterService,
              private toastrService: ToastrService,
              private router: Router) {
    this.formTitle = 'Recruiter Register';
  }
  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      name: this.name,
      email: this.email,
      passwordGroup: this.formBuilder.group({
        password: this.password,
        retypePassword: this.retypePassword,
      }, {validator: comparePassword})
    });
  }

  register(formdata: FormGroup) {
    this.recruiter = new Recruiter();
    this.recruiter.name = formdata.value.name;
    this.recruiter.email = formdata.value.email;
    this.recruiter.password = formdata.value.passwordGroup.password;

    this.recruiterService.createRecruiter(this.recruiter)
      .subscribe((response) => {
        console.log(response);
        if (response.success) {
          this.toastrService.success(response.message);
          this.router.navigate(['/recruiterProfile-login']);
        } else if (response.message) {
          this.toastrService.warning(response.message);
        } else {
          this.toastrService.error('Unknown server error. Please try again later.');
        }
      });
  }
}
