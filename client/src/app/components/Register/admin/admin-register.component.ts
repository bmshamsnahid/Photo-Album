import {Component, OnInit} from "@angular/core";
import {FormGroup, FormControl, Validators, AbstractControl, FormBuilder} from '@angular/forms';
import {Photographer} from "../../../model/photographer";
import {PhotographerService} from "../../../services/photographer.service";
import {ToastrService} from "../../../common/toastr.service";
import {Router} from "@angular/router";
import {Admin} from "../../../model/admin";
import {AdminService} from "../../../services/admin.service";

function comparePassword(c: AbstractControl): {[key: string]: boolean} | null {
  let passwordControl = c.get('password');
  let confirmControl = c.get('retypePassword');

  if (passwordControl.pristine || confirmControl.pristine) {
    return null;
  }

  if (passwordControl.value === confirmControl.value) {
    return null;
  }
  return { 'mismatchedPassword': true };
}
@Component({
  templateUrl: './admin-register.component.html',
  styleUrls: [
    './admin-register.component.css'
  ]
})
export class AdminRegisterComponent {
  formTitle: string;
  admin: Admin;
  registerForm: FormGroup;

  name = new FormControl('', [Validators.required]);
  email = new FormControl('', [Validators.email]);
  password = new FormControl('', [Validators.required, Validators.minLength(8),]);
  retypePassword = new FormControl('', [Validators.required]);

  constructor(private formBuilder: FormBuilder,
              private adminService: AdminService,
              private toastrService: ToastrService,
              private router: Router) {
    this.formTitle = 'Admin Register';
  }
  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      name: this.name,
      email: this.email,
      passwordGroup: this.formBuilder.group({
        password: this.password,
        retypePassword: this.retypePassword,
      }, {validator: comparePassword})
    });
  }

  register(formdata: FormGroup) {
    this.admin = new Admin();
    this.admin.name = formdata.value.name;
    this.admin.email = formdata.value.email;
    this.admin.password = formdata.value.passwordGroup.password;

    console.log('Admin: in component: ');
    console.log(this.admin);

    this.adminService.createAdmin(this.admin)
      .subscribe((response) => {
        if (response.success) {
          this.toastrService.success(response.message);
          this.router.navigate(['/adminProfile-login']);
        } else if (response.message) {
          this.toastrService.warning(response.message);
        } else {
          this.toastrService.error('Unknown server error. Please try again later.');
        }
      });
  }
}
