import {Component, OnInit} from "@angular/core";
import {User} from "../../../model/user";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {ToastrService} from "../../../common/toastr.service";
import {UserAuthService} from "../../../services/user-auth.service";
import {Router} from "@angular/router";

@Component({
  templateUrl: './admin-login.component.html',
  styleUrls: [
    './admin-login.component.css'
  ]
})
export class AdminLoginComponent implements OnInit {
  formTitle: string;
  user: User;
  loginForm: FormGroup;

  email = new FormControl('', [Validators.email]);
  password = new FormControl('', [Validators.required, Validators.minLength(8),]);

  constructor(private formBuilder: FormBuilder,
              // private toastrService: ToastrService,
              private userAuthService: UserAuthService,
              private router: Router) {
    this.formTitle = 'Admin Login';
  }
  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      email: this.email,
      password: this.password
    });
  }

  login(formdata: FormGroup) {
    this.user = new User();

    this.user.email = formdata.value.email;
    this.user.password = formdata.value.password;

    this.userAuthService.userLogin(this.user)
      .subscribe((response) => {
        if (this.userAuthService.isAdminLoggedIn()) {
          // this.toastrService.success('Admin Logged in');
          this.router.navigate(['/adminProfile-dashboard']);
        } else {
          // this.toastrService.warning('Admin not able to Logged in');
        }
      });
  }
}
