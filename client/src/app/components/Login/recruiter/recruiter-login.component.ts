import {Component, OnInit} from "@angular/core";
import {FormGroup, FormControl, Validators, AbstractControl, FormBuilder} from '@angular/forms';
import {Photographer} from "../../../model/photographer";
import {ToastrService} from "../../../common/toastr.service";
import {UserAuthService} from "../../../services/user-auth.service";
import {Router} from "@angular/router";
import {User} from "../../../model/user";

@Component({
  templateUrl: './recruiter-login.component.html',
  styleUrls: [
    './recruiter-login.component.css'
  ]
})
export class RecruiterLoginComponent implements OnInit {
  formTitle: string;
  user: User;
  loginForm: FormGroup;

  email = new FormControl('', [Validators.email]);
  password = new FormControl('', [Validators.required, Validators.minLength(8),]);

  constructor(private formBuilder: FormBuilder,
              private toastrService: ToastrService,
              private userAuthService: UserAuthService,
              private router: Router) {
    this.formTitle = 'Recruiter Login';
  }
  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      email: this.email,
      password: this.password
    });
  }

  login(formdata: FormGroup) {
    this.user = new User();

    this.user.email = formdata.value.email;
    this.user.password = formdata.value.password;

    this.userAuthService.userLogin(this.user)
      .subscribe((response) => {
        if (this.userAuthService.isRecruiterLoggedIn()) {
          this.toastrService.success('Recruiter Logged in');
          this.router.navigate(['/recruiterProfile-dashboard']);
        } else {
          this.toastrService.warning('Recruiter not able to Logged in');
        }
      });
  }
}
