import {Component, OnInit} from "@angular/core";
import {FormGroup, FormControl, Validators, AbstractControl, FormBuilder} from '@angular/forms';
import {Photographer} from "../../../model/photographer";
import {ToastrService} from "../../../common/toastr.service";
import {UserAuthService} from "../../../services/user-auth.service";
import {Router} from "@angular/router";
import {User} from "../../../model/user";

@Component({
  templateUrl: './photographer-login.component.html',
  styleUrls: [
    './photographer-login.component.css'
  ]
})
export class PhotographerLoginComponent implements OnInit {
  formTitle: string;
  user: User;
  loginForm: FormGroup;

  email = new FormControl('', [Validators.email]);
  password = new FormControl('', [Validators.required, Validators.minLength(8),]);

  constructor(private formBuilder: FormBuilder,
              private toastrService: ToastrService,
              private userAuthService: UserAuthService,
              private router: Router) {
    this.formTitle = 'Photographer Login';
  }
  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      email: this.email,
      password: this.password
    });
  }

  login(formdata: FormGroup) {
    this.user = new User();

    this.user.email = formdata.value.email;
    this.user.password = formdata.value.password;

    this.userAuthService.userLogin(this.user)
      .subscribe((response) => {
        if (this.userAuthService.isPhotographerLoggedIn()) {
          this.toastrService.success('Photographer Logged in');
          this.router.navigate(['/photographerProfile-dashboard']);
        } else {
          this.toastrService.warning('Photographer not able to Logged in');
        }
      });
  }
}
