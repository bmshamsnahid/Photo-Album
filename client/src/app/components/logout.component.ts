import {Component, OnInit} from "@angular/core";
import {UserAuthService} from "../services/user-auth.service";
import {Router} from "@angular/router";
import {ToastrService} from "../common/toastr.service";

@Component({
  template: ``
})
export class LogoutComponent implements OnInit {
  constructor(private userAuthService: UserAuthService,
              private router: Router,
              private tostrService: ToastrService) {}
  ngOnInit() {
    this.userAuthService.logout();
    this.tostrService.success('Successfully logged out.');
    this.router.navigate(['home']);
  }
}
