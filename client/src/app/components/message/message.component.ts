import {Component, OnInit} from "@angular/core";
import {MessageService} from "../../services/message.service";
import {ActivatedRoute} from "@angular/router";
import {ToastrService} from "../../common/toastr.service";
import {Message} from "../../model/message";

@Component({
  templateUrl: './message.component.html',
  styleUrls: [
    './message.component.css'
  ]
})
export class MessageComponent implements OnInit  {
  currentUserObj;
  currentUser;
  token;
  photographerId: string;

  messages: Message[];
  selectedMessage: Message;

  constructor (private messageService: MessageService,
               private activatedRouter: ActivatedRoute,
               private toastrService: ToastrService) {
    this.currentUserObj = JSON.parse(localStorage.getItem('currentUserObj'));
    this.selectedMessage = new Message();
    if (this.currentUserObj != null) {
      this.currentUser = this.currentUserObj.currentUser;
      this.photographerId = this.currentUser.photographerId;
      this.token = this.currentUserObj.token;
    }
  }
  ngOnInit() {
    this.messageService.allPhotographerMessage(this.photographerId)
      .subscribe((response) => {
        if (response.success) {
          this.messages = response.data;
          console.log(this.messages);
        } else if (response.message) {
          this.toastrService.warning(response.message);
        } else {
          this.toastrService.error('Fatal server error.');
        }
      });
  }

  onClickTable(message: Message) {
    console.log('Clicked: ');
    console.log(message);
    this.selectedMessage = message;
    message.isRead = true;
    this.messageService.updateMessage(message)
      .subscribe((response) => {
        if (response.success) {
          message.isRead = true;
        } else if (response.message) {
          this.toastrService.warning(response.message);
        } else {
          this.toastrService.error('Fatal server error.');
        }
      });
  }
}
