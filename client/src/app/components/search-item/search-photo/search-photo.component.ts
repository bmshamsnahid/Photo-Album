import {Component, OnInit} from "@angular/core";
import {ToastrService} from "../../../common/toastr.service";
import {ActivatedRoute} from "@angular/router";
import {PhotoService} from "../../../services/photo.service";
import {Photo} from "../../../model/photo";
import {environment} from "../../../../environments/environment";

@Component({
  templateUrl: './search-photo.component.html',
  styleUrls: [
    './search-photo.component.css'
  ]
})
export class SearchPhotoComponent implements OnInit {
  searchKeyword: string;
  photos: Photo[];
  baseUrl: string = environment.baseUrl;

  constructor(private toastrService: ToastrService,
              private activatedRoute: ActivatedRoute,
              private photoService: PhotoService) {}
  ngOnInit() {
    this.searchKeyword = this.activatedRoute.snapshot.params['keyword'];
    if (typeof this.searchKeyword == 'undefined' || this.searchKeyword.length <= 0) {
      this.toastrService.warning('Invalid or incomplete keyword')
    } else {
      console.log('Search Keyword: ' + this.searchKeyword);
      this.photoService.getSearchPhoto(this.searchKeyword)
        .subscribe((response) => {
          if (response.success) {
            this.photos = response.data;
            console.log('Searched photos');
            console.log(this.photos);
          } else if (response.message) {
            this.toastrService.warning(response.message);
          } else {
            this.toastrService.error('Fatal server error.')
          }
        });
    }
  }
}
