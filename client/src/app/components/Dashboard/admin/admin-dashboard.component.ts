import {Component, OnInit} from "@angular/core";
import {Photographer} from "../../../model/photographer";
import {PhotographerService} from "../../../services/photographer.service";
import {ToastrService} from '../../../common/toastr.service';
import {Router} from "@angular/router";

@Component({
  templateUrl: './admin-dashboard.component.html',
  styleUrls: [
    'admin-dashboard.component.css'
  ]
})
export class AdminDashboardComponent implements OnInit {
  photographers: Photographer[];
  constructor(private photographerService: PhotographerService,
              private toastrService: ToastrService,
              private router: Router) {}
  ngOnInit() {
    this.photographerService.getAllPhotographer()
      .subscribe((response) => {
        if (response.success) {
          this.photographers = response.data;
          console.log(this.photographers);
        } else if (response.message) {
          this.toastrService.warning(response.message);
        } else {
          this.toastrService.error('Fatal server error.');
        }
      });
  }

  onClickPhotographer(photographer: Photographer) {
    console.log(photographer);
    this.router.navigate([`adminProfile-dashboard/photographersInfo/${photographer._id}`]);
  }
}
