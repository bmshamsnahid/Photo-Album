import {Component, OnInit} from "@angular/core";
import {PhotographerService} from "../../../../services/photographer.service";
import {ToastrService} from "../../../../common/toastr.service";
import {AlbumService} from "../../../../services/album.service";
import {CategoryService} from "../../../../services/category.service";
import {Location} from '@angular/common';
import {Album} from "../../../../model/album";
import {Photographer} from "../../../../model/photographer";
import {AlbumInfo} from "../../../../model/albumInfo";
import {Category} from "../../../../model/category";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  templateUrl: './admin-dashboard-photographer-info.component.html',
  styleUrls: [
    './admin-dashboard-photographer-info.component.css'
  ]
})
export class AdminDashboardPhotographerInfoComponent implements OnInit {
  currentUserObj;
  currentUser;
  token: string;

  photographerId: string;
  photographer: Photographer;

  albums: Album[];
  albumsInfo: AlbumInfo[];
  selectedAlbumInfo: AlbumInfo;

  isAlbumSelected: boolean = false;

  selectedCategory: Category;
  categories: Category[];
  category: Category;

  constructor(private photographerService: PhotographerService,
              private toastrService: ToastrService,
              private albumService: AlbumService,
              private categoryService: CategoryService,
              private location: Location,
              private activatedRoute: ActivatedRoute,
              private router: Router) {
    this.selectedAlbumInfo = new AlbumInfo();
    this.selectedCategory = new Category();
    this.category = new Category();
    this.photographerId = this.activatedRoute.snapshot.params['photographerId'];
    console.log(this.photographerId);
  }

  ngOnInit() {
    console.log('admin dashboard onInit life cycye.');
    this.albumService.getPhotographerAlbum(this.photographerId)
      .subscribe((response) => {
        console.log('This is get photographer album info service response');
        console.log(response);
        if (response.success) {
          this.albumsInfo = response.data;
          console.log(this.albumsInfo);
        } else if (response.message) {
          this.toastrService.warning(response.message);
        } else {
          this.toastrService.error('Fatal server error.');
        }
      });
    this.categoryService.getCategory().subscribe((response) => {
      if (response.success) {
        this.categories = response.data;
      } else if (response.message) {
        this.toastrService.warning(response.message);
      } else {
        this.toastrService.error('Fatal server error in getting the categories');
      }
    });
  }

  onClickTable(albumInfo: AlbumInfo) {
    this.selectedAlbumInfo = albumInfo;
    this.selectedCategory = this.selectedAlbumInfo.albumCategory;
    console.log('Initial category:');
    console.log(this.selectedCategory);
    this.category = this.selectedAlbumInfo.albumCategory;
    this.isAlbumSelected = true;
    console.log(this.selectedAlbumInfo);
  }

  onChange(e) {
    console.log('Change category: ');
    console.log(this.selectedCategory);
    this.selectedAlbumInfo.album.category = this.selectedCategory._id;
  }
  onSelect(e, index) {
    this.selectedCategory = this.categories[index];
    console.log('Select category: ');
    console.log(this.selectedCategory);
  }
  updateAlbum() {
    console.log('Updating the album.');
    console.log(this.selectedAlbumInfo.album);
    this.albumService.updateAlbum(this.selectedAlbumInfo.album)
      .subscribe((response) => {
        if (response.success) {
          console.log(response.data);
          this.toastrService.success('Successfully updated the album.');
          location.reload();
        } else if (response.message) {
          this.toastrService.warning(response.message);
        } else {
          this.toastrService.error('Fatal server error');
        }
      });
  }
  setAlbumPrivacy(privacyFlag: boolean) {
    console.log('Album privacy: ' + privacyFlag);
    this.selectedAlbumInfo.album.isPrivate = privacyFlag;
  }
  browsePhotos() {
    console.log('Browsing photos');
    this.router.navigate([`/photographer-album-photo-dashboard//${this.selectedAlbumInfo.album._id}`]);
  }
  addPhotos() {
    this.router.navigate([`/photo-upload/${this.selectedAlbumInfo.album._id}`]);
  }

}
