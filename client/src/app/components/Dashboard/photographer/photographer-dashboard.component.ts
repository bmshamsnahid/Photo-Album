import {Component} from "@angular/core";
import {Photographer} from "../../../model/photographer";
import {AlbumService} from "../../../services/album.service";
import {ToastrService} from "../../../common/toastr.service";
import {Album} from "../../../model/album";
import {AlbumInfo} from "../../../model/albumInfo";
import {Category} from "../../../model/category";
import {CategoryService} from "../../../services/category.service";
import {Location} from '@angular/common';
import {Router} from "@angular/router";
import {PhotographerService} from "../../../services/photographer.service";

@Component({
  templateUrl: './photographer-dashboard.component.html',
  styleUrls: ['./photographer-dashboard.component.css']
})
export class PhotographerDashboardComponent {
  currentUserObj;
  currentUser;
  token: string;

  photographerId: string;
  photographer: Photographer;

  albums: Album[];
  albumsInfo: AlbumInfo[];
  selectedAlbumInfo: AlbumInfo;

  isAlbumSelected: boolean = false;

  selectedCategory: Category;
  categories: Category[];
  category: Category;

  constructor(private albumService: AlbumService,
              private toastrService: ToastrService,
              private categoryService: CategoryService,
              private location: Location,
              private photographeService: PhotographerService,
              private router: Router) {

    this.photographer = new Photographer();
    this.selectedAlbumInfo = new AlbumInfo();
    this.selectedCategory = new Category();
    this.category = new Category();
    this.currentUserObj = JSON.parse(localStorage.getItem('currentUserObj'));
    if (this.currentUserObj != null) {
      console.log('Not a null user');
      this.currentUser = this.currentUserObj.currentUser;
      this.photographerId = this.currentUser.photographerId;
      this.token = this.currentUserObj.token;
    } else {
      console.log('A null user');
    }
  }
  ngOnInit() {
    this.photographeService.getPhotographer(this.photographerId)
      .subscribe((response) => {
        if (response.success) {
          this.photographer = response.data;
          console.log(this.photographer);
          this.toastrService.success('Got the photographer');
        } else if (response.message) {
          this.toastrService.warning(response.data);
        } else {
          this.toastrService.error('Fatal server error.');
        }
      });

    this.albumService.getPhotographerAlbum(this.photographerId)
      .subscribe((response) => {
          if (response.success) {
            this.albumsInfo = response.data;
            console.log(this.albumsInfo);
          } else if (response.message) {
            this.toastrService.warning(response.message);
          } else {
            this.toastrService.error('Fatal server error.');
          }
      });
    this.categoryService.getCategory().subscribe((response) => {
      if (response.success) {
        this.categories = response.data;
      } else if (response.message) {
        this.toastrService.warning(response.message);
      } else {
        this.toastrService.error('Fatal server error in getting the categories');
      }
    });
  }

  onClickTable(albumInfo: AlbumInfo) {
    this.selectedAlbumInfo = albumInfo;
    this.selectedCategory = this.selectedAlbumInfo.albumCategory;
    console.log('Initial category:');
    console.log(this.selectedCategory);
    this.category = this.selectedAlbumInfo.albumCategory;
    this.isAlbumSelected = true;
    console.log(this.selectedAlbumInfo);
  }

  onChange(e) {
    console.log('Change category: ');
    console.log(this.selectedCategory);
    this.selectedAlbumInfo.album.category = this.selectedCategory._id;
  }
  onSelect(e, index) {
    this.selectedCategory = this.categories[index];
    console.log('Select category: ');
    console.log(this.selectedCategory);
  }
  updateAlbum() {
    console.log('Updating the album.');
    console.log(this.selectedAlbumInfo.album);
    this.albumService.updateAlbum(this.selectedAlbumInfo.album)
      .subscribe((response) => {
        if (response.success) {
          console.log(response.data);
          this.toastrService.success('Successfully updated the album.');
          location.reload();
        } else if (response.message) {
            this.toastrService.warning(response.message);
        } else {
            this.toastrService.error('Fatal server error');
        }
      });
  }
  setAlbumPrivacy(privacyFlag: boolean) {
    console.log('Album privacy: ' + privacyFlag);
    this.selectedAlbumInfo.album.isPrivate = privacyFlag;
  }
  browsePhotos() {
    console.log('Browsing photos');
    this.router.navigate([`/photographer-album-photo-dashboard//${this.selectedAlbumInfo.album._id}`]);
  }

  addPhotos() {
    this.router.navigate([`/photo-upload/${this.selectedAlbumInfo.album._id}`]);
  }
}
