import {Component, OnInit} from "@angular/core";
import {ActivatedRoute} from "@angular/router";
import {Location} from '@angular/common';
import {environment} from "../../../../../environments/environment";
import {Photo} from "../../../../model/photo";
import {ToastrService} from "../../../../common/toastr.service";
import {AlbumService} from "../../../../services/album.service";
import {PhotoService} from "../../../../services/photo.service";
import {CategoryService} from "../../../../services/category.service";
import {Category} from "../../../../model/category";

@Component({
  templateUrl: './album-photo-dashboard.component.html',
  styleUrls: [
    './album-photo-dashboard.component.css'
  ]
})
export class AlbumPhotoDashboardComponent implements OnInit {
  currentUserObj;
  currentUser;
  token: string;

  photographerId: string;
  photos: Photo[];
  baseUrl: string = environment.baseUrl;

  albumId: string;

  selectedPhoto: Photo;

  constructor(private activatedRoute: ActivatedRoute,
              private toastrService: ToastrService,
              private albumService: AlbumService,
              private photoService: PhotoService,
              private location: Location,
              private categoryService: CategoryService) {
    this.selectedPhoto = new Photo();
    this.albumId = this.activatedRoute.snapshot.paramMap.get('albumId');
    this.currentUserObj = JSON.parse(localStorage.getItem('currentUserObj'));
    if (this.currentUserObj != null) {
      this.currentUser = this.currentUserObj.currentUser;
      this.photographerId = this.currentUser.photographerId;
      this.token = this.currentUserObj.token;
    }
  }

  ngOnInit() {
    this.albumService.getAlbumPhoto(this.albumId)
      .subscribe((response) => {
        if (response.success) {
          this.photos = response.data;
          console.log(this.photos);
        } else if (response.message) {
          this.toastrService.warning(response.message);
        } else {
          this.toastrService.error('Fatal server error.');
        }
      });
  }

  onClickDownloadPhoto(photo: Photo) {
    window.open(this.baseUrl + photo.originalPath, "_blank");
  }

  onClickRemovePhoto(photo: Photo) {
    console.log('Removing the photo: ');
    console.log(photo);
    this.photoService.deletePhoto(photo)
      .subscribe((response) => {
        if (response.success) {
          this.toastrService.success('Successfully remode the photo.');
          console.log('Removed the photo');
          console.log(response);
          location.reload();
        } else if (response.message) {
          this.toastrService.warning(response.message);
        } else {
          this.toastrService.error('Fatal server error.');
        }
      });
  }

  onClickPhotoClick(photo: Photo) {
    this.selectedPhoto = photo;
    console.log('Photo is clicked for editing:');
    console.log(photo);
  }

  onClickPhotoUpdate(photo: Photo) {
    console.log('Selected photo: ');
    console.log(photo);
    this.photoService.updatePhoto(photo)
      .subscribe((response) => {
        if (response.success) {
          this.toastrService.success('Successfully updated the photo.');
          console.log(response.data);
        } else if (response.message) {
          this.toastrService.warning(response.message);
        } else {
          this.toastrService.error('Fatal server error.');
        }
      });
  }
}
