import {AfterViewInit, Component, OnInit} from "@angular/core";
import {Album} from "../../model/album";
import {Photographer} from "../../model/photographer";
import {AlbumService} from "../../services/album.service";
import {ActivatedRoute} from "@angular/router";
import {PhotographerService} from "../../services/photographer.service";
import {ToastrService} from "../../common/toastr.service";
import {AlbumInfo} from "../../model/albumInfo";


@Component({
  templateUrl: './albums.component.html',
  styleUrls: [
    'albums.component.css'
  ]
})
export class AlbumsComponent  implements OnInit {

  albums: Album[];
  albumsInfo: AlbumInfo[];
  categoryId: string;
  owner: Photographer;
  isDataLoaded: boolean = false;
  constructor (private albumService: AlbumService,
               private toastrService: ToastrService,
               private activatedRoute: ActivatedRoute,
               private photographerService: PhotographerService) {
  }
  ngOnInit() {
    this.albumService.getPublicAlbumsInfo()
      .subscribe((response) => {
        if (response.success) {
          this.albumsInfo = response.data;
          console.log(this.albumsInfo);
        } else if (response.message) {
          this.toastrService.warning(response.message);
        } else {
          this.toastrService.error('Fatal server error');
        }
      });
  }

  onClickAlbum(album: Album) {
    console.log('Clicked album.');
    console.log(album);
  }
}
