let express = require('express'),
    app = express(),
    bodyParser = require('body-parser'),
    morgan = require('morgan'),
    mongoose = require('mongoose'),
    cors = require('cors'),
    path = require('path'),
    cookieParser = require('cookie-parser'),
    ua = require('universal-analytics');

let config = require('./config');
let authController = require('./controller/auth');

let visitor = ua(config.analytics, {https: true});
visitor.pageview("/server").send();

let photoRoutes = require('./route/photo'),
    photographerRoutes = require('./route/photographer'),
    adminRoutes = require('./route/admin'),
    recruiterRoutes = require('./route/recruiter'),
    superAdminRoutes = require('./route/superAdmin'),
    authRoutes = require('./route/auth'),
    albumRoutes = require('./route/album'),
    messageRoutes = require('./route/message'),
    feedbackRoutes = require('./route/feedback'),
    categoryRoutes = require('./route/category'),
    stripeRoutes = require('./route/stripe'),
    clientRoutes = require('./route/client'),
    cartRoutes = require('./route/cart'),
    customRoutes = require('./route/custom/custom');

let port = process.env.PORT || config.serverPort;

mongoose.Promise = global.Promise;
mongoose.connect(config.database, (err) => {
    if (err) {
        console.log('Error in database connection. ' + err);
    } else {
        console.log('Database connected');
    }
});

app.use(morgan('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(cors());

app.use('/photo', photoRoutes);
app.use('/auth', authRoutes);
app.use('/photographer', photographerRoutes);
app.use('/admin', adminRoutes);
app.use('/recruiter', recruiterRoutes);
app.use('/superAdmin', superAdminRoutes);
app.use('/album', albumRoutes);
app.use('/message', messageRoutes);
app.use('/category', categoryRoutes);
app.use('/feedback', feedbackRoutes);
app.use('/stripe', stripeRoutes);
app.use('/client', clientRoutes);
app.use('/cart', cartRoutes);
app.use('/custom', customRoutes);
app.use('*', (req, res, next) => {
    res.sendFile(path.join(__dirname, 'public/index.html'));
});

app.listen(port, (err) => {
    if (err) {
        console.log('Error in listing port: ' + port);
    } else {
        console.log('App is running in port: ' + port);
    }
});
