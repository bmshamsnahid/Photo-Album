module.exports = {
    'serverPort': 8080,
    'tokenexp': 3600,
    'secret': 'mysecretkey',
    'analytics': 'UA-107924999-1',
    // 'database': 'mongodb://localhost:27017/photoAlbum',
    'database': 'mongodb://dbuser:dbpassword@ds159459.mlab.com:59459/photopher'
};