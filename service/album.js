var Album = require('../model/album');

const createAlbum = (req, res, next) => {

};

const getAlbum = (req, res, next) => {

};

const getAllAlbum = (req, res, next) => {

};

const getAlbumWithQuery = (req, res, next, query) => {

};

const updateAlbum = (req, res, next) => {

};

const deleteAlbum = (req, res, next) => {

};

module.exports = {
    createAlbum,
    getAlbum,
    getAllAlbum,
    getAlbumWithQuery,
    updateAlbum,
    deleteAlbum
};