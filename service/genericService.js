const createObject = (data, cb) => {
    data.save((err, data) => {
        if (err) {
            cb(err, null);
        } else {
            cb(null, data);
        }
    });
};

const getSingleObject = (data, id, cb) => {
    data.findById(id, (err, data) => {
        if (err) {
            cb(err, null);
        } else {
            cb(null, data);
        }
    });
};

const getAllObject = (data, cb) => {
    data.find((err, data) => {
        if (err) {
            cb(err, null);
        } else {
            cb(null, data);
        }
    });
};

const getObjectWithQuery = (data, query, cb) => {
    data.find(query, (err, data) => {
        if (err) {
            cb(err, null);
        } else {
            cb(null, data);
        }
    });
};

const updateObject = (data, cb) => {

};

const deleteObject = (data, id, cb) => {
    data.findByIdAndRemove(id, (err, data) => {
        if (err) {
            cb(err, null);
        } else {
            cb(null, data);
        }
    });
};

module.exports = {
    createObject,
    getSingleObject,
    getAllObject,
    getObjectWithQuery,
    updateObject,
    deleteObject
};