var Admin = require('../model/admin');

const createAdmin = (req, res, next) => {

};

const getAdmin = (req, res, next, cb) => {

};

const getAllAdmin = (req, res, next) => {

};

const getAdminWithQuery = (req, res, next, query) => {

};

const updateAdmin = (req, res, next) => {

};

const deleteAdmin = (req, res, next) => {

};

module.exports = {
  createAdmin,
  getAdmin,
  getAllAdmin,
  getAdminWithQuery,
  updateAdmin,
  deleteAdmin
};