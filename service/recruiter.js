var Recruiter = require('../model/recruiter');

const createRecruiter = (req, res, next) => {

};

const getRecruiter = (req, res, next) => {

};

const getAllRecruiter = (req, res, next) => {

};

const getRecruiterWithQuery = (req, res, next, query) => {

};

const updateRecruiter = (req, res, next) => {

};

const deleteRecruiter = (req, res, next) => {

};

module.exports = {
    createRecruiter,
    getRecruiter,
    getAllRecruiter,
    getRecruiterWithQuery,
    updateRecruiter,
    deleteRecruiter
};