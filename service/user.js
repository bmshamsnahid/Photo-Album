var User = require('../model/user');

const createUser = (req, res, next) => {

};

const getUser = (req, res, next) => {

};

const getAllUser = (req, res, next) => {

};

const getUserWithQuery = (req, res, next, query) => {

};

const updateUser = (req, res, next) => {

};

const deleteUser = (req, res, next) => {

};

module.exports = {
    createUser,
    getUser,
    getAllUser,
    getUserWithQuery,
    updateUser,
    deleteUser
};