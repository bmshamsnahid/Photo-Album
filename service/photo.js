const createPhoto = (data, cb) => {
    data.save((err, data) => {
        if (err) {
            cb(err, null);
        } else {
            cb(null, data);
        }
    });
};

const getPhoto = (req, res, next, cb) => {

    cb();
};

const getAllPhoto = (req, res, next) => {

};

const getPhotoWithQuery = (req, res, next, query) => {

};

const updatePhoto = (req, res, next) => {

};

const deletePhoto = (req, res, next) => {

};

module.exports = {
    createPhoto,
    getPhoto,
    getAllPhoto,
    getPhotoWithQuery,
    updatePhoto,
    deletePhoto
};