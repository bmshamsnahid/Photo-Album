var SuperAdmin = require('../model/auperAdmin');

const createSuperAdmin = (req, res, next) => {

};

const getSuperAdmin = (req, res, next) => {

};

const getAllSuperAdmin = (req, res, next) => {

};

const getSuperAdminWithQuery = (req, res, next, query) => {

};

const updateSuperAdmin = (req, res, next) => {

};

const deleteSuperAdmin = (req, res, next) => {

};

module.exports = {
    createSuperAdmin,
    getSuperAdmin,
    getAllSuperAdmin,
    getSuperAdminWithQuery,
    updateSuperAdmin,
    deleteSuperAdmin
};