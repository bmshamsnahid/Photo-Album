var Photographer = require('../model/photographer');

const createPhotographer = (req, res, next) => {

};

const getPhotographer = (req, res, next) => {

};

const getAllPhotographer = (req, res, next) => {

};

const getPhotographerWithQuery = (req, res, next, query) => {

};

const updatePhotographer = (req, res, next) => {

};

const deletePhotographer = (req, res, next) => {

};

module.exports = {
    createPhotographer,
    getPhotographer,
    getAllPhotographer,
    getPhotographerWithQuery,
    updatePhotographer,
    deletePhotographer
};