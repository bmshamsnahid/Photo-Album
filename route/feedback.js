let express = require('express'),
    router = express.Router(),
    feedbackController = require('../controller/feedback');

router.post('/', feedbackController.createFeedback);
router.get('/:feedbackId', feedbackController.getSingleFeedback);
router.get('/', feedbackController.getAllFeedback);
router.patch('/:feedbackId', feedbackController.updateFeedback);
router.delete('/:feedbackId', feedbackController.deleteFeedback);

module.exports = router;