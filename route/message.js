let express = require('express'),
    router = express.Router(),
    messageController = require('../controller/message');

router.get('/photographer/all/:photographerId', messageController.photographersAllMessage);
router.get('/photographer/read/:photographerId', messageController.photographersReadMessage);
router.get('/photographer/unread/:photographerId', messageController.photographersUnReadMessage);

router.post('/', messageController.createMessage);
router.get('/:messageId', messageController.getSingleMessage);
router.get('/', messageController.getAllMessage);
router.patch('/:messageId', messageController.updateMessage);
router.delete('/:messageId', messageController.deleteMessage);

module.exports = router;