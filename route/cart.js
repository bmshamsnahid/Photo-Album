let express = require('express'),
    router = express.Router(),
    cartController = require('../controller/cart');

router.get('/:photographerId', cartController.getCart);
router.get('/buyList/:photographerId', cartController.getBuyList);
router.get('/wishlist/:photographerId/:photoId', cartController.removePhoto);
router.get('/buyList/:photographerId/:photoId', cartController.passToBuyList);

module.exports = router;