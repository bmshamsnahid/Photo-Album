var express = require('express'),
    router = express.Router(),
    photographerController = require('../controller/photographer');

router.post('/', photographerController.createPhotographer);
router.get('/:photographerId', photographerController.getSinglePhotographer);
router.get('/', photographerController.getAllPhotographer);
router.patch('/:photographerId', photographerController.updatePhotographer);
router.delete('/:photographerId', photographerController.deletePhotographer);

module.exports = router;
