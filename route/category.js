let express = require('express'),
    router = express.Router(),
    categoryController = require('../controller/category');

router.post('/', categoryController.createCategory);
router.get('/:categoryId', categoryController.getSingleCategory);
router.get('/', categoryController.getAllCategory);
router.patch('/:categoryId', categoryController.updateCategory);
router.delete('/:categoryId', categoryController.deleteCategory);

module.exports = router;