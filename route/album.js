let express = require('express'),
    router = express.Router(),
    albumController = require('../controller/album');

router.get('/public', albumController.getPublicAlbums);
router.get('/category/:categoryId', albumController.getCategorizedAlbums);
router.get('/photographer/:photographerId', albumController.getPhotographerAlbum);
router.get('/publicAlbumsInfo', albumController.getPublicAlbumsInfo);
router.get('/photographersPublicAlbumsInfo/:photographerId', albumController.getPhotographerPublicAlbumsInfo);


router.post('/', albumController.createAlbum);
router.get('/:albumId', albumController.getSingleAlbum);
router.get('/', albumController.getAlbums);
router.patch('/:albumId', albumController.updateAlbum);
router.delete('/:albumId', albumController.deleteAlbum);



module.exports = router;