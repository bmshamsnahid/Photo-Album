var express = require('express'),
    router = express.Router(),
    photoController = require('../controller/photo');

router.get('/album/:albumId', photoController.getPhotoOfAlbum);
router.get('/public', photoController.getPublicPhoto);
router.get('/search/:searchKeyword', photoController.searchPhoto);


router.post('/upload/:id', photoController.createPhoto);
router.get('/', photoController.getAllPhoto);
router.get('/:photoId', photoController.getSinglePhoto);
router.post('/', photoController.createPhoto);
router.patch('/:photoId', photoController.updatePhoto);
router.delete('/:photoId', photoController.deletePhoto);

module.exports = router;
